<?php
include 'sesion.php';
date_default_timezone_set('America/Bogota');
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/programacion.php');

if (isset($_GET["idprog"]))
{
  $idprog = $_GET["idprog"];
  $displayboton = "none";
  $displaytablas = "block";
  $en_fecha = "disabled";
}
else
{
  $idprog = 0;
  $displayboton = "block";
  $displaytablas = "none";
  $en_fecha="";
}

$det_programacion = new programacion;
$fecha = $det_programacion -> consulta_fecha_programacion($idprog);
$tiempototal = $det_programacion -> consulta_tiempo_total_programacion($idprog);
$thoras = round($tiempototal/60,2);
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Programación de Trabajo</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/rowgroup/1.1.0/css/rowGroup.dataTables.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script src=".\js\crea_prog.js"></script>
  <script>
    var idprog= <?php echo $idprog;?>;
    $(document).ready(function()
    {
      oTable = $('#detalle_prog').dataTable({
          "order": [[1, "desc" ]],
          "sDom": 'T<"clear">lfrtip',
          "bAutoWidth": false,
          "paging":   false,
          "bFilter": false,
          "bProcessing": true,
          "bServerSide": false,
          "aaSorting": [[ 0, "asc" ]],
          "bInfo": false,
          "sAjaxSource": "./tablas/programacion.php?idprog=<?php echo $idprog;?>",
          "oLanguage":{
              "sProcessing":   "Procesando...",
              "sZeroRecords":  "No se encontraron resultados",
              "sSearch":       "Buscar:",
              "sUrl":          "",
              "sCopy":		 "Se han copiado los datos al portapapeles",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sPrevious": "Anterior",
                  "sNext":     "Siguiente",
                  "sLast":     "&Uacute;ltimo",
              }
          },
          "columnDefs": [
            {
              "targets": [1],
              "visible": false,
              "searchable": false
            }
          ],
          rowGroup: {
            startRender: function (rows,group) 
            {
                var tiempot = rows
                    .data()
                    .pluck(3)
                    .reduce( function (a, b) {
                        return a + b*1;
                    }, 0);

              return $('<tr/>')
                .append('<td colspan="5">'+group+'--> Tiempo: '+tiempot.toFixed(1)+' minutos</td>')
            },
            endRender: null,
            dataSrc: 1
          }
      });

      oTable2 = $('#items_pendientes').dataTable({
          "sDom": 'T<"clear">lfrtip',
          "bAutoWidth": false,
          "paging":   false,
          "bFilter": false,
          "bProcessing": true,
          "bServerSide": false,
          "aaSorting": [[ 0, "asc" ]],
          "bInfo": false,
          "sAjaxSource": "./tablas/items_pendientes.php?fechamax=<?php echo $fecha;?>",
          "oLanguage":{
              "sProcessing":   "Procesando...",
              "sZeroRecords":  "No se encontraron resultados",
              "sSearch":       "Buscar:",
              "sUrl":          "",
              "sCopy":		 "Se han copiado los datos al portapapeles",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sPrevious": "Anterior",
                  "sNext":     "Siguiente",
                  "sLast":     "&Uacute;ltimo",
              }
          }
      });
    });
  </script>
</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->

    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-2">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Programación de Trabajo</h1>
              </div>
              <form class="user" name="form_programacion" id="form_programacion" rol="form" method="POST" action=".\controller\crea_programacion.php">
                <div class="form-group">
                    <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Fecha" value=<?php echo $fecha; echo " ".$en_fecha;?> >
                    <span class="help-block" id="error"></span>
                </div>
                <button id="crea" style="display:<?php echo $displayboton;?>;" type="submit" class="btn btn-primary btn-user btn-block">Ver</button>
                <hr>
                </form>
                <div id="tablas" style="display:<?php echo $displaytablas;?>;">
                    <div class="card bg-success text-white shadow">
                        <div class="card-body">
                            Tiempo total programado:
                            <h1 id="tiempo_total" class="text-right text-dark"><?php echo $tiempototal; ?> minutos (<?php echo $thoras;?> horas)</h1>
                        </div>
                    </div>
                    <button class="btn btn-secondary" onclick="agrega_prendas_dia($('#fecha').val());">Agregar prendas del día</button>
                    <a href="valida_programacion.php?idprog=<?php echo $idprog;?>" class="btn btn-secondary btn-icon-split <?php if($_SESSION['rol_usuario'] == '2') echo " disabled"?>">
                        <span class="icon text-white-50">
                          <i class="fas fa-arrow-right"></i>
                        </span>
                        <span class="text">Validar / Terminar</span>
                    </a>
                    <a href="registro_programacion.php" class="btn btn-secondary btn-icon-split">
                        <span class="icon text-white-50">
                          <i class="fas fa-arrow-right"></i>
                        </span>
                        <span class="text">Crear otra programación</span>
                    </a>
                    <hr>
                    <div class="form-group">
                        <div class="table-responsive-sm">
                            <h6 class="m-0 font-weight-bold text-primary">Detalle de la programación</h6>
                            <table class="table table-condensed table-sm" id="detalle_prog" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>Prenda</th>
                                    <th>Trabajo</th>
                                    <th>Complejidad</th>
                                    <th>Tiempo Estimado<br>(minutos)</th>
                                    <th>Fecha Entrega</th>
                                    <th>Cliente</th>
                                    <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td colspan="7" class="dataTables_empty">---</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr><hr>
                    <div class="form-group">
                        <div class="table-responsive-sm">
                            <h6 class="m-0 font-weight-bold text-primary">Pendientes por programar</h6>
                            <table class="table table-condensed table-sm" id="items_pendientes" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>Prenda</th>
                                    <th>Trabajo</th>
                                    <th>Complejidad</th>
                                    <th>Tiempo Estimado<br>(minutos)</th>
                                    <th>Fecha Entrega</th>
                                    <th>Cliente</th>
                                    <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td colspan="7" class="dataTables_empty">---</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/rowgroup/1.1.0/js/dataTables.rowGroup.min.js"></script>
  <script> 
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#programar").addClass("active");
      });
    });
    </script> 
</body>
</html>