<?php
include 'sesion.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/usuario.php');

$action = 0;
if (isset($_GET["idusuario"]))
{
  $idusuario = $_GET["idusuario"];
  $action = 1;
}
else
{
  $idusuario = 0;
}

if($idusuario != 0)
{
  $usuario = new usuario;
  $detusuario = $usuario->traer_datos_usuario($idusuario);
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registro de Usuarios</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>

  <script src=".\js\registro_usuario.js"></script>
  <script>
    <?php 
      if($idusuario != 0)
      {
        echo "var rol = ".$detusuario[0]['id_rol'];
      }
      else
      {
        echo "var rol = null";
      }
    ?>
  </script>
</head>

<body class="bg-gradient-primary">

  <div id="wrapper">

    <!-- Incluir menu lateral-->
    <div id="includedMenu"></div>
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
    
      <!-- Main Content -->
      <div id="content">
    
        <!-- Incluir menu superior-->
    
        <!-- Begin Page Content -->
        <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-5 d-none d-lg-block">
            <img src="img/ITEJEC_logo.png" alt="" />
          </div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Crea un Usuario</h1>
              </div>
              <div id="exito" class="alert alert-info" role="alert" style="display:none;">
                El usuario ha sido creado/editado exitosamente!
              </div>
              <div id="falla" class="alert alert-danger" role="alert" style="display:none;">
                El usuario no ha sido creado/editado, por favor inténtelo nuevamente!
              </div>
              <form class="user" name="form_usuario" id="form_usuario" rol="form" action=".\controller\crea_usuario.php?idusuario=<?php echo $idusuario;?>&action=<?php echo $action;?>" method="POST">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre y Apellidos" value="<?php echo $detusuario[0]['nombre'];?>">
                    <span class="help-block" id="error"></span>   
                  </div>
                  <div class="col-sm-6">
                    <input type="tel" class="form-control" id="cedula" name="cedula" placeholder="cédula" value=<?php echo $detusuario[0]['cedula'];?>>
                    <span class="help-block" id="error"></span>   
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" value="<?php echo $detusuario[0]['correo'];?>">
                  <span class="help-block" id="error"></span>   
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="<?php echo $detusuario[0]['direccion'];?>">
                    <span class="help-block" id="error"></span>   
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="tel" class="form-control" id="telefono1" name="telefono1" placeholder="Teléfono" value=<?php echo $detusuario[0]['telefono1'];?>>
                      <span class="help-block" id="error"></span>   
                    </div>
                    <div class="col-sm-6">
                      <input type="tel" class="form-control" id="telefono2" name="telefono2" placeholder="Teléfono Alternativo" value=<?php echo $detusuario[0]['telefono2'];?>>
                      <span class="help-block" id="error"></span>   
                    </div>  
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <select name="rol" id="rol" class="form-control " placeholder="Seleccione rol">
                            <option value="1">Administrador</option>
                            <option value="2" selected default>Cajero / Programador</option>
                            <option value="3">Alistador</option>
                        </select>
                        <span class="help-block" id="error"></span>   
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="nombre_usuario" name="nombre_usuario" placeholder="Usuario" value=<?php echo $detusuario[0]['usuario'];?>>
                      <!--<input type="text" class="form-control " id="nombre_usuario" name="nombre_usuario" placeholder="Usuario" onblur="verifica_usuario();">-->
                      <div id="usuario_ya_existe" style="display:none;" class="text-center alert alert-danger" role="alert">
                        Este usuario ya existe
                      </div>
                      <span class="help-block" id="error"></span>   
                    </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                    <span class="help-block" id="error"></span>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control" id="repeatpassword" name="repeatpassword" placeholder="Repita Contraseña">
                    <span class="help-block" id="error"></span>   
                  </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">Registrar</button>
              </form>
              <hr>
              <!--<div class="text-center">
                <a class="small" href="forgot-password.html">Olvidó su contraseña?</a>
              </div>-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
  <script> 
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#usuarios").addClass("active");
      });
    });
    </script> 
</body>
</html>