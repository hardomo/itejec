<?php
include 'sesion.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/tipo_trabajo.php');

$action = 0;
if (isset($_GET["idtt"]))
{
  $idtt = $_GET["idtt"];
  $action = 1;
}
else
{
  $idtt = 0;
}

if($idtt != 0)
{
  $tt = new tipo_trabajo;
  $dettt = $tt->traer_datos_tt($idtt);
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registro de Tipos de Trabajo</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src=".\js\registro_tipo_trabajo.js"></script>
</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->


    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Tipo de Trabajo</h1>
              </div>
              <div id="exito" class="alert alert-info" role="alert" style="display:none;">
                El tipo de trabajo ha sido creado exitosamente!
              </div>
              <div id="falla" class="alert alert-danger" role="alert" style="display:none;">
                El tipo de trabajo no ha sido creado, por favor inténtelo nuevamente!
              </div>
              <form class="user" name="form_tipo_trabajo" id="form_tipo_trabajo" rol="form" action=".\controller\crea_tipo_trabajo.php?idtt=<?php echo $idtt;?>&action=<?php echo $action;?>" method="POST">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Tipo de Trabajo" value="<?php echo $dettt[0]['nombre'];?>">
                        <span class="help-block" id="error"></span>   
                    </div>
                    <div class="col-sm-6">
                        <textarea type="tel" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción"><?php echo $dettt[0]['descripcion'];?></textarea>
                        <span class="help-block" id="error"></span>   
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="number" class="form-control" id="valor" name="valor" placeholder="Valor en pesos" value="<?php echo $dettt[0]['valor'];?>">
                        <span class="help-block" id="error"></span>   
                    </div>
                    <div class="col-sm-6">
                        <input type="number" class="form-control" id="tiempo" name="tiempo" placeholder="Tiempo en minutos que toma realizar el trabajo" value="<?php echo $dettt[0]['tiempo'];?>">
                        <span class="help-block" id="error"></span>   
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">Registrar Tipo de Trabajo</button>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
  <script> 
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#tipoTrabajo").addClass("active");
      });
    });
    </script> 
</body>
</html>