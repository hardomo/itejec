<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class clientes_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_clientes()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, cedula, telefono1, id FROM cliente WHERE nombre NOT LIKE '%0%' ORDER BY nombre;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function crea_cliente($nombre,$cedula,$correo,$direccion,$telefono1,$telefono2,$fecha_nac)
    {
        $cadena_campos = "";
        $cadena_valores = "";
        if($direccion)
        {
            $cadena_campos.= ",direccion";
            $cadena_valores.=",'".$direccion."'";
        }
        if($telefono1)
        {
            $cadena_campos.= ",telefono1";
            $cadena_valores.=",".$telefono1;
        }
        if($telefono2)
        {
            $cadena_campos.= ",telefono2";
            $cadena_valores.=",".$telefono2;
        }
        if($fecha_nac)
        {
            $cadena_campos.= ",fecha_nacimiento";
            $cadena_valores.=",'".$fecha_nac."'";
        }
        $cedula = "'".$cedula."'";

        //Validar formato de nombre
        //if ($preg = preg_match("^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$",utf8_encode($nombre))){
        $preg = preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ]/',utf8_encode($nombre));
        if($preg === 0 || $preg === false){
            //echo "regex error ".$nombre;
            //var_dump($preg);
            return 0;
        }  
        

        if($this->db->conectar())
        {
            $sql = "INSERT INTO cliente (nombre,cedula,correo_e".$cadena_campos.")
            VALUES ('".$nombre."',$cedula,'".$correo."'".$cadena_valores.")";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear el cliente: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_cliente_id($idcliente)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT * FROM cliente WHERE id = ".$idcliente;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function edita_cliente($idcliente,$nombre,$cedula,$correo,$direccion,$telefono1,$telefono2,$fecha_nac)
    {
        $cadena_campos = "";
        if($direccion)
        {
            $cadena_campos.= ", direccion= '".$direccion."'";
        }
        else
        {
            $cadena_campos.= ", direccion= null";
        }
        if($telefono1)
        {
            $cadena_campos.= ", telefono1= ".$telefono1;
        }
        if($telefono2)
        {
            $cadena_campos.= ", telefono2= ".$telefono2;
        }
        if($fecha_nac)
        {
            $cadena_campos.= ", fecha_nacimiento= '".$fecha_nac."'";
        }
        else
        {
            $cadena_campos.= ", fecha_nacimiento= null";
        }
        if($this->db->conectar())
        {
            $sql = "UPDATE cliente
            SET nombre='".$nombre."', cedula = ".$cedula.$cadena_campos.
            " WHERE id = ".$idcliente;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo editar el cliente: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente editado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_id_cliente_x_cedula($cedula)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT id FROM cliente WHERE cedula = ".$cedula." LIMIT 1";
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila['id'];
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }
}
?>