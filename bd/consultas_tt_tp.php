<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class tt_tp_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_detalles_programacion($idprog)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre as prenda, tipo_trabajo.nombre as trabajo, complejidad.nombre as complejidad, tipo_trabajo.tiempo+complejidad.incremento_tiempo AS tiempo_estimado,orden_trabajo.fecha_entrega, item_orden_trabajo.id as id_item
            FROM item_orden_trabajo
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            WHERE programacion.id = ".$idprog." ORDER BY tipo_trabajo.id";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function consulta_fecha_programacion($idprog)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT fecha
            FROM programacion
            WHERE id = ".$idprog;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_items_atrasados($fecha)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre as prenda, tipo_trabajo.nombre as trabajo, complejidad.nombre as complejidad, tipo_trabajo.tiempo+complejidad.incremento_tiempo AS tiempo_estimado,orden_trabajo.fecha_entrega, item_orden_trabajo.id as id_item
            FROM item_orden_trabajo
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            WHERE orden_trabajo.fecha_entrega <= '".$fecha."'
            AND item_orden_trabajo.programacion IS null
            ORDER BY orden_trabajo.fecha_entrega ASC, orden_trabajo.id";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function consulta_items_pendientes_programacion($fecha)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre as prenda, tipo_trabajo.nombre as trabajo, complejidad.nombre as complejidad, tipo_trabajo.tiempo+complejidad.incremento_tiempo AS tiempo_estimado,orden_trabajo.fecha_entrega, item_orden_trabajo.id as id_item
            FROM item_orden_trabajo
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            WHERE (orden_trabajo.fecha_entrega IS null
            OR orden_trabajo.fecha_entrega > '".$fecha."')
            AND item_orden_trabajo.programacion IS null
            ORDER BY orden_trabajo.fecha_entrega ASC, orden_trabajo.id";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function consulta_tiempo_total_programacion($idprog)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT SUM(tipo_trabajo.tiempo+complejidad.incremento_tiempo) AS tiempo_total
            FROM item_orden_trabajo
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            WHERE item_orden_trabajo.programacion = ".$idprog;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function desprograma_item($id)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo
            SET programacion = null
            WHERE id = ".$id;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo desprogramar el item: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Item desprogramado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function programa_item($id_item,$id_prog)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo
            SET programacion = ".$id_prog.
            " WHERE id = ".$id_item;
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear la prenda: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function programa_dia($fecha,$idprog)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            SET item_orden_trabajo.programacion = ".$idprog."
             WHERE item_orden_trabajo.programacion IS null AND orden_trabajo.fecha_entrega = '".$fecha."'";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_existe_programacion_fecha($fecha)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT id
            FROM programacion
            WHERE fecha = '".$fecha."';";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                //echo "No se pudo realizar la consulta: ". mysql_error();
                return 0;
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function crea_programacion($fecha)
    {
        if($this->db->conectar())
        {
            $sql = "INSERT into programacion (fecha)
            VALUES ('".$fecha."');";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear la programación: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Programación creada exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_detalles_validar_programacion($idprog)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre as prenda, tipo_trabajo.nombre as trabajo, complejidad.nombre as complejidad, tipo_trabajo.tiempo+complejidad.incremento_tiempo AS tiempo_estimado,orden_trabajo.fecha_entrega, url_foto, color, observaciones, estado_item.nombre AS estado, item_orden_trabajo.id as id_item
            FROM item_orden_trabajo
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            LEFT JOIN estado_item ON item_orden_trabajo.estado = estado_item.id
            WHERE programacion.id = ".$idprog;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function valida_prog($idprog)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo 
            SET estado = 3
            WHERE programacion = ".$idprog.";";
            //echo $sql;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo validar la programaci�n: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Programaci�n creada exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_detalles_asignar_contenedor($idprog)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre as prenda, tipo_trabajo.nombre as trabajo, orden_trabajo.fecha_entrega, url_foto, color, marca, observaciones, item_orden_trabajo.id as id_item, orden_trabajo.id as id_ot, contenedor.id as contenedor
            FROM item_orden_trabajo
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            LEFT JOIN estado_item ON item_orden_trabajo.estado = estado_item.id
            LEFT JOIN contenedor ON item_orden_trabajo.contenedor = contenedor.id
            WHERE programacion.id = ".$idprog." ORDER BY orden_trabajo.id, item_orden_trabajo.id";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function asigna_contenedor_item($id_item,$contenedor)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo
            SET contenedor = ".$contenedor."
            WHERE id = ".$id_item;
            //echo $sql;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear la prenda: ". mysql_error();
                $this->db->desconectar();
                return 0;
                //return $sql;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_contenedor()
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT * FROM contenedor ORDER BY nombre";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function termina_prog($idprog)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo 
            SET estado = 4
            WHERE programacion = ".$idprog.";";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function termina_item($iditem)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo 
            SET estado = 4
            WHERE id = ".$iditem.";";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }
}
?>