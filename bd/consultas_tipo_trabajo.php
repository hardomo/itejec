<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class tipo_trabajo_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_tipos_trabajo()
    {
        if($this->db->conectar())
        {
            $sql = "select * from tipo_trabajo ORDER BY nombre ASC;";
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function crea_tipo_trabajo($nombre,$descripcion,$valor,$tiempo)
    {
        if($this->db->conectar())
        {
            $sql = "INSERT INTO tipo_trabajo (nombre,descripcion,valor,tiempo)
            VALUES ('".$nombre."','".$descripcion."',".$valor.",".$tiempo.")";
            //echo $sql;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear el tipo de trabajo: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Tipo de trabajo creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_tt_id($idtt)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, descripcion, valor, tiempo, id
            FROM tipo_trabajo 
            WHERE id = ".$idtt;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function edita_tipo_trabajo($idtt,$nombre,$descripcion,$valor,$tiempo)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE tipo_trabajo
            SET nombre='".$nombre."',descripcion = '".$descripcion."',valor = ".$valor.",tiempo = ".$tiempo.
            " WHERE id = ".$idtt;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo editar el cliente: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente editado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function elimina_tipo_trabajo($idtt)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM tipo_trabajo 
            WHERE id = ".$idtt;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_tipos_trabajo_rep()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, descripcion, valor, tiempo, id FROM tipo_trabajo ORDER BY nombre ASC;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }
}
?>