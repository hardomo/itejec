<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class cartera_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_resumen_cartera($fecha1,$fecha2)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT fecha_ingreso,SUM(valor) as valor, SUM(abonos) as abonos, (SUM(valor)-SUM(abonos)) as saldo, COUNT(fecha_ingreso) as num_ordenes
            FROM
            (SELECT orden_trabajo.id, orden_trabajo.fecha_ingreso, SUM(item_orden_trabajo.valor) as valor
            FROM item_orden_trabajo
            JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo=orden_trabajo.id
            WHERE (orden_trabajo.fecha_ingreso BETWEEN '".$fecha1."' AND '".$fecha2."')
            GROUP BY  orden_trabajo.id) as tabla1

            LEFT JOIN

            (SELECT orden_trabajo.id, SUM(abono.valor) as abonos
            FROM abono
            JOIN orden_trabajo ON abono.orden_trabajo=orden_trabajo.id
            WHERE (orden_trabajo.fecha_ingreso BETWEEN '".$fecha1."' AND '".$fecha2."')
            GROUP BY orden_trabajo.id) as tabla2
            on tabla1.id=tabla2.id
            GROUP BY fecha_ingreso;";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }
}

?>