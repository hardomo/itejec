<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class complejidad_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_complejidades()
    {
        if($this->db->conectar())
        {
            $sql = "select * from complejidad ORDER BY nombre ASC;";
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function crea_complejidad($nombre,$incremento_porcentual,$incremento_tiempo)
    {
        if($this->db->conectar())
        {
            $sql = "INSERT INTO complejidad (nombre,incremento_porcentual,incremento_tiempo)
            VALUES ('".$nombre."',".$incremento_porcentual.",".$incremento_tiempo.")";
            //echo $sql;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear la complejidad: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Complejidad creada exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_comp_id($idcomp)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, incremento_porcentual, incremento_tiempo, id
            FROM complejidad 
            WHERE id = ".$idcomp;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function edita_complejidad($idcomp,$nombre,$incremento_porcentual,$incremento_tiempo)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE complejidad
            SET nombre='".$nombre."',incremento_porcentual = ".$incremento_porcentual.",incremento_tiempo = ".$incremento_tiempo.
            " WHERE id = ".$idcomp;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo editar el cliente: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente editado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function elimina_complejidad($idcomp)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM complejidad 
            WHERE id = ".$idcomp;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_complejidades_rep()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, incremento_porcentual, incremento_tiempo, id FROM complejidad ORDER BY nombre ASC;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }
}
?>