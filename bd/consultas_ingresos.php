<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class ingresos_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_resumen_ingresos($fecha1,$fecha2)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT abono.fecha as fecha, SUM(abono.valor) as valor, COUNT(fecha) as numero
            FROM abono
            JOIN orden_trabajo ON abono.orden_trabajo = orden_trabajo.id
            WHERE abono.fecha BETWEEN '".$fecha1."' AND '".$fecha2."'
            GROUP BY fecha
            ORDER BY abono.fecha ASC;";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function consulta_abonos_fecha($fecha)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT abono.fecha as fecha, cliente.nombre as cliente, abono.valor as valor_abono, orden_trabajo.id as orden
            FROM abono
            JOIN orden_trabajo ON abono.orden_trabajo = orden_trabajo.id
            JOIN cliente ON orden_trabajo.cliente = cliente.id
            WHERE abono.fecha = '".$fecha."'
            ORDER BY abono.fecha ASC;";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }
}
?>