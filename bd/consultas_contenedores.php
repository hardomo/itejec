<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class contenedor_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function crea_contenedor($nombre)
    {
        if($this->db->conectar())
        {
            $sql = "INSERT INTO contenedor (nombre)
            VALUES ('".$nombre."')";
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear el contenedor: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Contenedor creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_contenedor_id($idcontenedor)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, id
            FROM contenedor
            WHERE id = ".$idcontenedor;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function edita_contenedor($idcontenedor,$nombre)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE contenedor
            SET nombre='".$nombre."'".
            " WHERE id = ".$idcontenedor;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function elimina_contenedor($idcontenedor)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM contenedor 
            WHERE id = ".$idcontenedor;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_contenedores_rep()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, id FROM contenedor ORDER BY nombre ASC;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }
}
?>