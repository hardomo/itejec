<?php
    require_once "conectar2.php";

class usuarios_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    /*public function consulta_usuarios()
    {
        if($this->db->conectar())
        {
            $sql = "select * from usuario;";
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
                exit;
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            mysql_free_result($this->consulta);
            $this->db->desconectar();
            return $this->result;
        }
    }*/

    public function consulta_usuarios()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre_usuario as usuario, usuario.nombre, cedula, correo, direccion,telefono1,telefono2, rol_usuario.nombre as rol, usuario.id
            FROM usuario JOIN rol_usuario ON usuario.rol = rol_usuario.id
            ORDER BY nombre_usuario;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }


    public function crea_usuario($nombre,$nombre_usuario,$cedula,$correo,$direccion,$telefono1,$telefono2,$password,$rol)
    {
        $cadena_campos = "";
        $cadena_valores = "";
        if($telefono2)
        {
            $cadena_campos.= ",telefono2";
            $cadena_valores.=",".$telefono2;
        }
        if($this->db->conectar())
        {
            $sql = "INSERT INTO usuario (nombre,nombre_usuario,cedula,correo,direccion,telefono1,password,rol".$cadena_campos.")
            VALUES ('".$nombre."','".$nombre_usuario."',$cedula,'".$correo."','".$direccion."',$telefono1,'".sha1($password)."',$rol".$cadena_valores.")";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear el usuario: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Usuario creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_usuario_id($idusuario)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre_usuario as usuario, usuario.nombre, cedula, correo, direccion,telefono1,telefono2, rol_usuario.nombre as nombre_rol, rol as id_rol, usuario.id
            FROM usuario JOIN rol_usuario ON usuario.rol = rol_usuario.id
            WHERE usuario.id = ".$idusuario;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_opciones_rol()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT * FROM rol_usuario;";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function edita_usuario($idusuario,$nombre,$nombre_usuario,$cedula,$correo,$direccion,$telefono1,$telefono2,$password,$rol)
    {
        $cadena_campos = "";
        if($telefono2)
        {
            $cadena_campos.= ", telefono2= ".$telefono2;
        }
        else
        {
            $cadena_campos.= ", telefono2= null";
        }
        if($password)
        {
            $cadena_campos.= ", password= '".sha1($password)."'";
        }
        if($this->db->conectar())
        {
            $sql = "UPDATE usuario
            SET nombre='".$nombre."', cedula = ".$cedula.",nombre_usuario= '".$nombre_usuario."',correo= '".$correo."',direccion= '".$direccion."',telefono1= ".$telefono1.",rol=".$rol.$cadena_campos.
            " WHERE id = ".$idusuario;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo editar el cliente: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente editado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function elimina_usuario($idusuario)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM usuario 
            WHERE id = ".$idusuario;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }
}
?>