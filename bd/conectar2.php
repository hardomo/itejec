<?php
//require_once 'config.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/config.php');

class BaseDatos
{
    public $conexion;
    protected $db;

    public function conectar()
    {
        $this->conexion = mysql_connect(HOST, USER, PASS);
        if ($this->conexion == 0) DIE("Lo sentimos, no se ha podido conectar con MYSQL: " . mysql_error());
        $this->db = mysql_select_db(DBNAME, $this->conexion);
        if ($this->db == 0) DIE("No se ha podido conectar con la base datos: ".DBNAME);
        mysql_set_charset('utf8', $this->conexion);
        return true;
    }

    public function desconectar()
    {
        if ($this->conectar->conexion) 
        {
            mysql_close($this->$conexion);
        }
    }

    public function pruebadb()
    {
        $tabla = "usuario";
        $query = mysql_query("SELECT count(*) from $tabla", $this->conexion);
        if ($query == 0) echo "Sentencia incorrecta llamando a tabla: $tabla.";
        else 
        {
            $nregistrostotal = mysql_result($query, 0, 0);
            echo "Hay $nregistrostotal registros en la tabla: $tabla.";
            mysql_free_result($query);
        }
    }
}