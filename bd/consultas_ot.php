<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class ot_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_items_ot_id($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre AS prenda,
                tipo_trabajo.nombre AS tipo_trabajo,
                item_orden_trabajo.valor,
                programacion.fecha as programacion,
                contenedor.nombre as contenedor,
                estado_item.nombre AS estado,
                item_orden_trabajo.marca AS marca,
                item_orden_trabajo.id AS id
            FROM item_orden_trabajo
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN contenedor ON item_orden_trabajo.contenedor = contenedor.id
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN estado_item ON item_orden_trabajo.estado = estado_item.id
            WHERE orden_trabajo.id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();

        }
    }

    public function consulta_items_etiqueta($iditem)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre AS prenda,
                tipo_trabajo.nombre AS tipo_trabajo,
                item_orden_trabajo.color AS color,
                item_orden_trabajo.valor,
                item_orden_trabajo.marca,
                programacion.fecha as programacion,
                contenedor.nombre as contenedor,
                estado_item.nombre AS estado,
                item_orden_trabajo.id AS id
            FROM item_orden_trabajo
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN contenedor ON item_orden_trabajo.contenedor = contenedor.id
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN estado_item ON item_orden_trabajo.estado = estado_item.id
            WHERE item_orden_trabajo.id = ".$iditem;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_items_ot_id_array($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT tipo_prenda.nombre AS prenda,
                tipo_trabajo.nombre AS tipo_trabajo,
                item_orden_trabajo.color AS color,
                item_orden_trabajo.valor,
                programacion.fecha as programacion,
                contenedor.nombre as contenedor,
                estado_item.nombre AS estado,
                item_orden_trabajo.id AS id
            FROM item_orden_trabajo
            LEFT JOIN orden_trabajo ON item_orden_trabajo.orden_trabajo = orden_trabajo.id
            LEFT JOIN complejidad ON item_orden_trabajo.complejidad = complejidad.id
            LEFT JOIN tipo_trabajo ON item_orden_trabajo.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN tipo_prenda ON item_orden_trabajo.tipo_prenda = tipo_prenda.id
            LEFT JOIN contenedor ON item_orden_trabajo.contenedor = contenedor.id
            LEFT JOIN programacion ON item_orden_trabajo.programacion = programacion.id
            LEFT JOIN estado_item ON item_orden_trabajo.estado = estado_item.id
            WHERE orden_trabajo.id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_detalles_cliente_ot($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();

            $sql = "SELECT cliente.id AS id_cliente, fecha_ingreso, fecha_entrega, estado_ot.nombre AS estado_ot, cliente.nombre, cedula, direccion, telefono1, telefono2, correo_e, fecha_nacimiento, programacion.id AS idprog
            FROM orden_trabajo
            LEFT JOIN cliente ON orden_trabajo.cliente = cliente.id
            LEFT JOIN estado_ot ON orden_trabajo.estado = estado_ot.id
            LEFT JOIN programacion ON orden_trabajo.fecha_entrega = programacion.fecha
            WHERE orden_trabajo.id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_abonos_ot($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT SUM(valor) AS abonos
            FROM abono
            LEFT JOIN orden_trabajo ON abono.orden_trabajo = orden_trabajo.id
            WHERE orden_trabajo.id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result = $fila['abonos'];
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_valor_total_ot($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT SUM(valor) AS total
            FROM item_orden_trabajo
            WHERE orden_trabajo = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result = $fila['total'];
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_complejidad()
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT * FROM complejidad";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_tipo_prenda()
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT * FROM tipo_prenda ORDER BY nombre ASC";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_tipo_trabajo()
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT * FROM tipo_trabajo ORDER BY nombre ASC";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function agrega_prenda_ot($datos,$idot)
    {
        if($this->db->conectar())
        {
            $datos['marca'] = substr($datos['marca'],0,59);
            $sql = "INSERT into item_orden_trabajo (tipo_prenda,tipo_trabajo,complejidad,orden_trabajo,url_foto,color,talla,marca,observaciones,valor,estado)
            VALUES (".$datos['tipo_prenda'].",".$datos['tipo_trabajo'].",".$datos['complejidad'].",".$idot.",'".$datos['url_foto']."','".$datos['color']."','".$datos['talla']."','".$datos['marca']."','".$datos['observaciones']."',".$datos['valor'].",1)";
            //echo $sql;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            $this->db->desconectar();
            return mysql_insert_id();

            /*if (!$this -> consulta)
            {
                //echo "No se pudo crear la prenda: ". mysql_error();
                $this->db->desconectar();
                return 0;
                //return $sql;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }*/
        }
    }

    public function consulta_clientes_select()
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT id, nombre
                    FROM cliente
                    ORDER BY nombre ASC";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function crea_ot($cliente,$fingreso,$fentrega)
    {
        if($this->db->conectar())
        {
            if($fentrega)
            {
                $fentrega = "'".$fentrega."'";
            }
            else
            {
                $fentrega = "null";
            }
            $sql = "INSERT into orden_trabajo (cliente,fecha_ingreso,fecha_entrega,estado)
            VALUES ($cliente,'".$fingreso."',".$fentrega.",1)";
            //echo $sql;
            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                //echo "No se pudo crear la orden de trabajo: ". mysql_error();
                $this->db->desconectar();
                return 0;
                //return $sql;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_ultima_ot_cliente($cliente)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT id
            FROM orden_trabajo
            WHERE cliente = ".$cliente." ORDER BY id DESC LIMIT 1";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function agrega_abono_ot($datos)
    {
        if($this->db->conectar())
        {
            $sql = "INSERT into abono (orden_trabajo,valor,fecha)
            VALUES (".$datos['idot'].",".$datos['valorabono'].",'".$datos['fechaabono']."')";
            echo $sql;
            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                echo "No se pudo crear el abono: ". mysql_error();
                $this->db->desconectar();
                return 0;
                //return $sql;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_cliente_id($idcliente)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT * FROM cliente WHERE id = ".$idcliente;
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                //echo $sql;
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            //echo $sql;
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_ordenes_cliente($idcliente)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT fecha_ingreso, fecha_entrega, estado, valor_total, SUM(abono.valor) as abonos, idot
            FROM (SELECT orden_trabajo.fecha_ingreso as fecha_ingreso, orden_trabajo.fecha_entrega as fecha_entrega, estado_ot.nombre as estado, SUM(item_orden_trabajo.valor) as valor_total, orden_trabajo.id as idot
            FROM orden_trabajo
            LEFT JOIN estado_ot ON orden_trabajo.estado = estado_ot.id
            LEFT JOIN item_orden_trabajo ON orden_trabajo.id = item_orden_trabajo.orden_trabajo
            WHERE cliente = ".$idcliente.
            " GROUP BY orden_trabajo.id) AS tabla1
            LEFT JOIN abono ON abono.orden_trabajo = tabla1.idot
            GROUP BY idot";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function entrega_ot($idot)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE orden_trabajo
            SET estado = 4
            WHERE id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function entrega_items_ot($idot)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE item_orden_trabajo
            SET estado = 5
            WHERE orden_trabajo = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function edita_f_entrega_ot($idot,$fecha)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE orden_trabajo
            SET fecha_entrega = '".$fecha."' WHERE id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_detalles_item($iditem)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT * FROM item_orden_trabajo WHERE id = ".$iditem;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function actualiza_item($detalles,$iditem)
    {
        if($this->db->conectar())
        {
            $cadena_campos = "";
            if($detalles['url_foto'])
            {
                $cadena_campos.= ", url_foto= '".$detalles['url_foto']."'";
            }
            else
            {
                $cadena_campos.= ", url_foto= null";
            }
            if($detalles['color'])
            {
                $cadena_campos.= ", color= '".$detalles['color']."'";
            }
            else
            {
                $cadena_campos.= ", color= null";
            }
            if($detalles['talla'])
            {
                $cadena_campos.= ", talla= '".$detalles['talla']."'";
            }
            else
            {
                $cadena_campos.= ", talla= null";
            }
            if($detalles['observaciones'])
            {
                $cadena_campos.= ", observaciones= '".$detalles['observaciones']."'";
            }
            else
            {
                $cadena_campos.= ", observaciones= null";
            }

            if($detalles['marca'])
            {
                $cadena_campos.= ", marca= '".$detalles['marca']."'";
            }
            else
            {
                $cadena_campos.= ", marca= null";
            }


            $sql = "UPDATE item_orden_trabajo
            SET tipo_prenda = ".$detalles['tipo_prenda'].
            ", tipo_trabajo = ".$detalles['tipo_trabajo'].
            ", complejidad = ".$detalles['complejidad'].
            ", valor = ".$detalles['valor']. $cadena_campos
            ." WHERE id = ".$iditem;

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                $this->db->desconectar();
                return 1;
            }
        }
    }

    public function elimina_item($iditem)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM item_orden_trabajo WHERE id = ".$iditem;

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_detalle_abonos_ot($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT abono.fecha, abono.valor, abono.id
            FROM abono
            LEFT JOIN orden_trabajo ON abono.orden_trabajo = orden_trabajo.id
            WHERE orden_trabajo.id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function edita_abono_ot($params)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE abono
            SET fecha = '".$params['fechaabono']."', valor = ".$params['valorabono']." WHERE id = ".$params['idabonoeditar'].";";

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function confirma_ot($idot)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE orden_trabajo
            SET estado = 5
            WHERE id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_id_items_ot($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT id FROM item_orden_trabajo
            WHERE orden_trabajo =  ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function elimina_ot($idot)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM orden_trabajo WHERE id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);

            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_estado_ot_id($idot)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT estado_ot.nombre as estado
            FROM orden_trabajo
            LEFT JOIN estado_ot ON orden_trabajo.estado = estado_ot.id
            WHERE orden_trabajo.id = ".$idot;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_ordenes_fecha($fecha)
    {
        if($this->db->conectar())
        {
            $this-> result = array();
            $sql = "SELECT fecha_ingreso, fecha_entrega, cliente, estado, valor_total, SUM(abono.valor) as abonos, idot
            FROM (SELECT orden_trabajo.fecha_ingreso as fecha_ingreso, orden_trabajo.fecha_entrega as fecha_entrega, cliente.nombre as cliente, estado_ot.nombre as estado, SUM(item_orden_trabajo.valor) as valor_total, orden_trabajo.id as idot
            FROM orden_trabajo
            LEFT JOIN estado_ot ON orden_trabajo.estado = estado_ot.id
            LEFT JOIN item_orden_trabajo ON orden_trabajo.id = item_orden_trabajo.orden_trabajo
            LEFT JOIN cliente ON orden_trabajo.cliente = cliente.id
            WHERE fecha_ingreso = '".$fecha."' GROUP BY orden_trabajo.id) AS tabla1
            LEFT JOIN abono ON abono.orden_trabajo = tabla1.idot
            GROUP BY idot";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }
}
?>