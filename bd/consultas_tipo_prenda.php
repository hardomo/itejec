<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/conectar2.php');

class tipo_prenda_model
{
    private $db;
    private $result;
    private $consulta;

    public function __construct()
    {
        $this-> db = new BaseDatos();
        $this-> result = array();
    }

    public function consulta_tipos_prenda()
    {
        if($this->db->conectar())
        {
            $sql = "select * from tipo_prenda ORDER BY nombre ASC;";
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function crea_tipo_prenda($nombre)
    {
        if($this->db->conectar())
        {
            $sql = "INSERT INTO tipo_prenda (nombre)
            VALUES ('".$nombre."')";
            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo crear el tipo de prenda: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Tipo de prenda creado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_tp_id($idtp)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, id
            FROM tipo_prenda
            WHERE id = ".$idtp;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function edita_tipo_prenda($idtp,$nombre)
    {
        if($this->db->conectar())
        {
            $sql = "UPDATE tipo_prenda
            SET nombre='".$nombre."'".
            " WHERE id = ".$idtp;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                //echo "No se pudo editar el cliente: ". mysql_error();
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    //echo "Cliente editado exitosamente";
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function elimina_tipo_prenda($idtp)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM tipo_prenda 
            WHERE id = ".$idtp;

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
    }

    public function consulta_tipos_prenda_rep()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT nombre, id FROM tipo_prenda ORDER BY nombre ASC;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }
	
	public function consulta_tipos_trabajo_tprenda($tprenda)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT tipo_trabajo.id as ttid, tipo_trabajo.nombre as tt, tipo_trabajo.valor
            FROM tipo_trabajo_tipo_prenda 
            LEFT JOIN tipo_trabajo ON tipo_trabajo_tipo_prenda.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN tipo_prenda ON tipo_trabajo_tipo_prenda.tipo_prenda =  tipo_prenda.id
            WHERE tipo_trabajo_tipo_prenda.tipo_prenda = $tprenda;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                while($fila = mysql_fetch_assoc($this->consulta))
                {
                    $this->result[] = $fila;
                }
            }
            $this->db->desconectar();
            return $this->result;
        }
    }

    public function consulta_tipos_trabajo_tprenda_rep($tprenda)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT tipo_trabajo.nombre as trabajo, tipo_trabajo.id as id
            FROM tipo_trabajo_tipo_prenda 
            LEFT JOIN tipo_trabajo ON tipo_trabajo_tipo_prenda.tipo_trabajo = tipo_trabajo.id
            LEFT JOIN tipo_prenda ON tipo_trabajo_tipo_prenda.tipo_prenda =  tipo_prenda.id
            WHERE tipo_trabajo_tipo_prenda.tipo_prenda = $tprenda;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function consulta_tipos_trabajo_rep()
    {
        if($this->db->conectar())
        {
            $sql = "SELECT tipo_trabajo.nombre as trabajo, tipo_trabajo.id as id
            FROM tipo_trabajo;";
            
            $this -> consulta = mysql_query($sql, $this->db->conexion);
            if (!$this -> consulta)
            {
                echo "No se pudo realizar la consulta: ". mysql_error();
            }
            else
            {
                return $this->consulta;
            }
            $this->db->desconectar();
        }
    }

    public function consulta_tt_asignado($idtt,$idtp)
    {
        if($this->db->conectar())
        {
            $sql = "SELECT * FROM tipo_trabajo_tipo_prenda
            WHERE tipo_trabajo = $idtt AND tipo_prenda = $idtp";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                echo "Error de consulta:".$sql."\n";
                $this->db->desconectar();
                return 0;
            }
            else
            {
                $this->db->desconectar();
                if(mysql_affected_rows()>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else{
            echo "No se puede conectar a la DB\n";
        }
    }

    //Elimina las asignaciones de tipos de trabajo:
    public function borra_trabajos($idtp)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM tipo_trabajo_tipo_prenda WHERE tipo_prenda = $idtp";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                echo "Error de consulta:".$sql."\n";
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
        else{
            echo "No se puede conectar a la DB\n";
        }
    }

    public function asigna_trabajo($idtt,$idtp)
    {
        if($this->db->conectar())
        {
            $sql = "INSERT INTO tipo_trabajo_tipo_prenda (tipo_trabajo, tipo_prenda)
            VALUES ($idtt,$idtp);";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                echo "Error de consulta:".$sql."\n";
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
        else{
            echo "No se puede conectar a la DB\n";
        }
    }

    public function desasigna_trabajo($idtt,$idtp)
    {
        if($this->db->conectar())
        {
            $sql = "DELETE FROM tipo_trabajo_tipo_prenda
            WHERE tipo_trabajo = $idtt AND tipo_prenda = $idtp;";

            $this -> consulta = mysql_query($sql, $this->db->conexion);
           
            if (!$this -> consulta)
            {
                echo "Error de consulta:".$sql."\n";
                $this->db->desconectar();
                return 0;
            }
            else
            {
                if(mysql_affected_rows()>0)
                {
                    $this->db->desconectar();
                    return 1;
                }
            }
        }
        else{
            echo "No se puede conectar a la DB\n";
        }
    }

}
?>