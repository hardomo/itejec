<?php
@session_start();
?>

    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-cut "></i>
        </div>
        <div class="sidebar-brand-text mx-3">ilo</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Heading -->
        <div class="sidebar-heading">
          Clientes
        </div>

        <!-- Nav Item - Dashboard
        <li class="nav-item active">  -->
        <li id="inicio" class="nav-item">
          <a class="nav-link <?php if($_SESSION['rol_usuario'] == '3') echo " disabled"?>" href="consulta_cliente.php">
            <i class="fas fa-fw fa-user-circle"></i>
            <span>Clientes</span></a>
        </li>
        <li id="registrarCliente" class="nav-item">
          <a class="nav-link <?php if($_SESSION['rol_usuario'] == '3') echo " disabled"?>" href="registro_cliente.php">
            <i class="fas fa-fw fa-address-card "></i>
            <span>Nuevo Cliente</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">
      }


      <!-- Heading -->
      <div class="sidebar-heading">
        Programación
      </div>

      <!-- Nav Item - Dashboard -->
      <li id="programar" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] == '3') echo " disabled"?>" href="registro_programacion.php">
          <i class="fas fa-fw fa-tasks  "></i>
          <span>Programar</span></a>
      </li>

      <li id="validar" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] == '2') echo " disabled"?>" href="valida_programacion.php">
          <i class="fas fa-fw fa-tasks  "></i>
          <span>Validar</span></a>
      </li>";

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Terminación
      </div>

      <li id="terminar" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] == '2') echo " disabled"?>" href="confirma_terminacion.php">
          <i class="fas fa-fw fa-tasks  "></i>
          <span>Confirmar Terminación</span></a>
      </li>

      <li id="ubicar" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] == '2') echo " disabled"?>" href="asigna_contenedor.php">
          <i class="fas fa-fw fa-tasks  "></i>
          <span>Ubicar en contenedores</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Prendas
      </div>

      <!-- Nav Item - Dashboard -->
      <li id="tipoPrenda" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] == '3') echo " disabled"?>" href="reporte_tipos_prenda.php">
          <i class="fas fa-fw fa-tshirt "></i>
          <span>Tipos de Prenda</span></a>
      </li>

      <li id="tipoTrabajo" class="nav-item">
          <a class="nav-link <?php if($_SESSION['rol_usuario'] == '3') echo " disabled"?>" href="reporte_tipos_trabajo.php">
            <i class="fas fa-fw fa-cut  "></i>
            <span>Tipos de Trabajo</span></a>
      </li>

      <li id="mcomplejidad" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] != '1') echo " disabled"?>" href="reporte_complejidades.php">
          <i class="fas fa-fw fa-cut  "></i>
          <span>Complejidades</span></a>
    </li>


      <li id="contenedor" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] == '2') echo " disabled"?>" href="reporte_contenedores.php">
          <i class="fas fa-fw fa-cut  "></i>
          <span>Contenedores</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Administrador
      </div>

      <!-- Nav Item - Dashboard -->
      <li id="usuarios" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] != '1') echo " disabled"?>" href="reporte_usuarios.php">
          <i class="fas fa-fw fa-users  "></i>
          <span>Usuarios</span></a>
      </li>

      <!-- Nav Item - Dashboard -->
      <li id="informes" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] != '1') echo " disabled"?>" href="reporte_cartera.php">
          <i class="fas fa-fw fa fa-chart-line"></i>
          <span>Informe de Cartera</span></a>
      </li>

      <!-- Nav Item - Dashboard -->
      <li id="ingresos" class="nav-item">
        <a class="nav-link <?php if($_SESSION['rol_usuario'] != '1') echo " disabled"?>" href="reporte_ingresos_fechas.php">
          <i class="fas fa-fw fa fa-chart-line"></i>
          <span>Informe de Ingresos</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

       <!-- Heading -->
       <div class="sidebar-heading">
        Sesión
      </div>

      <!-- Nav Item - Dashboard -->
      <li id="salir" class="nav-item">
        <a class="nav-link" href="salir.php">
          <i class="fas fa-fw fa-sign-out"></i>
          <span>Cerrar sesión</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
