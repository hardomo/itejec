<?php
include 'sesion.php';
date_default_timezone_set('America/Bogota');
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/tipo_prenda.php');

$prenda = new tipo_prenda;
if(isset($_GET["idtp"]))
{
    $idtp = $_GET["idtp"];
}
else
{
    $idtp = 0;
}
$det_prenda = $prenda->traer_datos_tp($idtp);
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Asignación de tipos de trabajo a tipo de prenda: <?php echo $det_prenda[0]['nombre']; ?></title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script src=".\js\asigna_tt_tp.js?v=1.0"></script>
  <script>
    var idtp= <?php echo $idtp;?>;
    $(document).ready(function()
    {
      oTable = $('#detalle_tt_tp').dataTable({
          "sDom": 'T<"clear">lfrtip',
          "bAutoWidth": false,
          "paging":   false,
          //"bFilter": false,
          "bProcessing": true,
          "bServerSide": false,
          "aaSorting": [[ 0, "asc" ]],
          "bInfo": false,
          "sAjaxSource": "./tablas/detalle_tt_tp.php?idtp=<?php echo $idtp;?>",
          "oLanguage":{
              "sProcessing":   "Procesando...",
              "sZeroRecords":  "No se encontraron resultados",
              "sSearch":       "Buscar:",
              "sUrl":          "",
              "sCopy":		 "Se han copiado los datos al portapapeles",
              "oPaginate": {
                "sFirst":    "Primero",
                "sPrevious": "Anterior",
                "sNext":     "Siguiente",
                "sLast":     "&Uacute;ltimo",
              }
          },
          "drawCallback": function(settings) {
            $('[data-toggle="tooltip"]').tooltip();
          }
      });
    }
  );
  </script>
</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->

    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-2">
                <h1 class="h4 text-gray-900 mb-4">Asignación de tipos de trabajo a tipo de prenda: <?php echo $det_prenda[0]['nombre']; ?></h1>
                <div class="btn-group">
                  <button type="button" class="btn btn-secondary" onclick="termina_seleccion();">Guardar cambios</button>
                </div>
                <hr>
                <div class="form-group">
                    <div class="table-responsive-sm">
                        <h6 class="m-0 font-weight-bold text-primary">Detalle de trabajos asignados</h6>
                        <table class="table table-condensed table-sm" id="detalle_tt_tp" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th>Trabajo</th>
                                <th>Asignar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td colspan="2" class="dataTables_empty">---</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/plug-ins/1.10.20/api/fnFilterClear.js"></script>

  <script> 
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#terminar").addClass("active");
      });
    });
    </script> 

    <div id="dialog5" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Guardado</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>¡Los trabajos han sido asignados exitosamente!</p>
                    </div>
                    <div class="modal-footer">
                    </div>
                  </div>
                </div>
              </div>
</body>
</html>