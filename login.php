<?php
@session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_ip.php');

$ip = $_SERVER['REMOTE_ADDR'];
$ip2 = $_SERVER['HTTP_X_FORWARDED_FOR'];
//echo "REMOTE_ADDR:".$ip."<br>";
//echo "HTTP_X_FORWARDED_FOR:".$ip2."<br>";

$consultas_ip = new consultas_ip();
$estado_ip = intval($consultas_ip->consulta_ip($ip));
if(!is_numeric($estado_ip)){
  $estado_ip = 0;
}
//echo "total ip:".$estado_ip."<br>";
$errorip = 0;
if(isset($_GET['errorip'])){
  $errorip = $_GET['errorip'];
}
//echo "errorip: ".$errorip."<br>";

?>

<!DOCTYPE html>
<html lang="es">
<head>
  <!--<meta charset="utf-8">-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Itejec - Sistema de Gestión de Órdenes de Trabajo - Inicio de Sesión</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous">
  </script>
</head>

<body class="bg-gradient-primary">
  <script>
    $(document).ready(function()
    {
      console.log("ready");
      if(window.location.href.indexOf("error=1")>=0)
      {
        $('#error').show();
        console.log("error");
      }
      else
      {
        console.log("sin error");
      }
    });
  </script>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block">
                <img src="img/ilo_logo.png" alt="" />
              </div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Bienvenido!</h1>
                  </div>
                  <form class="user" action="login2.php" method="post">
                    <div class="form-group">
                      <input type="text" <?php if($estado_ip == 0) echo " disabled"?> class="form-control form-control-user" id="usuario" name="usuario" aria-describedby="emailHelp" placeholder="Usuario">
                    </div>
                    <div class="form-group">
                      <input type="password" <?php if($estado_ip == 0) echo " disabled"?> class="form-control form-control-user" id="pass" name="password" placeholder="Contraseña">
                    </div>
                    <!--<div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Recordar mis datos de acceso</label>
                      </div>
                    </div>-->
                    <button type="submit" <?php if($estado_ip == 0) echo " disabled"?> class="btn btn-primary btn-user btn-block">Ingresar</button>
                  </form>
                  <hr>
                  <div id="error" style="display:none;" class="text-center alert alert-danger" role="alert">
                  Datos de acceso incorrectos
                  </div>
                  <div id="registroip" <?php if($estado_ip != 0) echo "style=\"display:none;\""?>>
                  <div id="error" class="text-center alert alert-danger" role="alert">
                    La IP <?php echo $ip;?> no tiene autorización para conexión. Ingrese los datos de administrador para agregar la IP a la lista permitida.
                  </div> 
                    <form class="user"  action=".\controller\agregarip.php?ip=<?php echo $ip;?>" method="post">
                      <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="usuarioip" name="usuarioip" aria-describedby="emailHelp" placeholder="Usuario Admin">
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control form-control-user" id="passip" name="passwordip" placeholder="Contraseña">
                      </div>
                      <!--<div class="form-group">
                        <div class="custom-control custom-checkbox small">
                          <input type="checkbox" class="custom-control-input" id="customCheck">
                          <label class="custom-control-label" for="customCheck">Recordar mis datos de acceso</label>
                        </div>
                      </div>-->
                      <button type="submit" class="btn btn-primary btn-user btn-block">Registrar IP</button>
                    </form>
                    <div id="error" <?php if($errorip == 0) echo " style=\"display:none;\""?> class="text-center alert alert-danger" role="alert">
                    Los datos son incorrectos o el usuario no tiene permisos de administrador.
                  </div> 
                  </div>
                  <!--<div class="text-center">
                    <a class="small" href="forgot-password.html">Olvidó su contraseña?</a>
                  </div>-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
</body>
</html>