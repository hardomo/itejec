<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_programacion.php');
$model = new programacion_model();
$rResult = $model->consulta_detalles_asignar_contenedor($_GET['idprog']) or die(mysql_error());

/*function traer_opciones_contenedor($item,$contenedor)
{
    $opciones_contenedor = $this->model->consulta_contenedor();

    $op_contenedor = "<select onchange='asignar_contenedor(".$item.",$(this).val();)'>";
    for($i=0; $i<count($opciones_contenedor); $i++)
    {
        if($contenedor == $opciones_contenedor[$i]['id'])
        {
            $op_contenedor.="<option value=".$opciones_contenedor[$i]['id']." selected>".$opciones_contenedor[$i]['nombre']."</option>";
        }
        else
        {
            $op_contenedor.="<option value=".$opciones_contenedor[$i]['id'].">".$opciones_contenedor[$i]['nombre']."</option>";
        }
    }
    $op_contenedor.= "</select>";
    return $op_contenedor;
}*/

$columnas = array(
'prenda',
'trabajo',
'fecha_entrega',
'url_foto',
'color',
'marca',
'observaciones',
'id_item',
'id_ot',
'contenedor',
'cliente');
    
$output = array(
    "aaData" => array()
);

while ($aRow = mysql_fetch_array($rResult, MYSQL_ASSOC))
{
    $row = array();
    for ($i=0 ; $i<count($columnas)-1; $i++)
    {
        if($columnas[$i] == "id_item")
        {
            $item = $aRow[$columnas[$i]];
            $contenedor = $aRow['contenedor'];
            $opciones_contenedor = $model->consulta_contenedor();
            $op_contenedor = "<select onchange='asignar_contenedor(".$item.",$(this).val());'>";
            if(!is_null($contenedor) || $contenedor == 0)
            {
                $op_contenedor.= "<option value='' selected></option>";
            }
            for($k=0; $k<count($opciones_contenedor); $k++)
            {
                if($contenedor == $opciones_contenedor[$k]['id'])
                {
                    $op_contenedor.="<option value=".$opciones_contenedor[$k]['id']." selected>".$opciones_contenedor[$k]['nombre']."</option>";
                }
                else
                {
                    $op_contenedor.="<option value=".$opciones_contenedor[$k]['id'].">".$opciones_contenedor[$k]['nombre']."</option>";
                }
            }
            $op_contenedor.= "</select>";
            $row[] = $op_contenedor;
        }
        else if($columnas[$i] == "url_foto" && strlen($aRow[$columnas[$i]])>0)
        {
            //$row[] = "<button id=\"ver_foto\" type=\"button\" class=\"btn btn-secondary\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\" title=\"\" data-original-title=\"<img src='foto/img/".$aRow[$columnas[$i]]."'></img>\">Ver</button>";
            $row[] = "<button id='ver_foto' class='btn btn-secondary' data-toggle='tooltip' data-placement='bottom' data-html='true' title=\"<img src='foto/img/".$aRow[$columnas[$i]]."'/>\">Ver</button>";
        }
        else
        {
            $row[] = $aRow[$columnas[$i]];
        }
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
?>