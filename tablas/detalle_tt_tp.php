<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_tipo_prenda.php');
$model = new tipo_prenda_model();
$rResult = $model->consulta_tipos_trabajo_tprenda_rep($_GET['idtp']) or die(mysql_error());
$ttrabajos = $model->consulta_tipos_trabajo_rep();

$columnas = array(
'trabajo',
'id');
    
$output = array(
    "aaData" => array()
);
$chequeados = array();

while ($aRow = mysql_fetch_array($rResult, MYSQL_ASSOC))
{
    $row = array();
    for ($i=0 ; $i<count($columnas); $i++)
    {
        if($columnas[$i] == "id")
        {
            $row[] ="<input type='checkbox' checked='true' name='".$aRow[$columnas[$i]]."' id='".$aRow[$columnas[$i]]."' onchange=''>";
        }
        else
        {
            $row[] = $aRow[$columnas[$i]];
        }
    }
    $output['aaData'][] = $row;
    $chequeados[]=$aRow;
}

while ($aRow = mysql_fetch_array($ttrabajos, MYSQL_ASSOC))
{
    $row = array();
    if(!in_array($aRow,$chequeados))
    {
        for ($i=0 ; $i<count($columnas); $i++)
        {
            if($columnas[$i] == "id")
            {
                $row[] ="<input type='checkbox' name='".$aRow[$columnas[$i]]."' id='".$aRow[$columnas[$i]]."' onchange=''>";
            }
            else
            {
                $row[] = $aRow[$columnas[$i]];
            }
        }
        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
?>