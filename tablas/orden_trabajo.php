<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_ot.php');
$model = new ot_model();
$estado_orden = $model->consulta_estado_ot_id($_GET['idot'])[0]['estado'];
$rResult = $model->consulta_items_ot_id($_GET['idot']) or die(mysql_error());

$columnas = array(
'prenda',
'marca',
'tipo_trabajo',
'valor',
'programacion',
'contenedor',
'estado',
'id');
    
$output = array(
    "aaData" => array()
);
    
while ($aRow = mysql_fetch_array($rResult, MYSQL_ASSOC))
{
    $row = array();
    for ($i=0 ; $i<count($columnas); $i++)
    {
        if($columnas[$i] == "id")
        {
            if($estado_orden == "Pendiente")
            {
                $row[] = "<a href=\"registro_prenda.php?iditem=".$aRow[$columnas[$i]]."&accion=1\"><img style='width: 24px; height: 24px;' src=\"./img/png/edit.png\" title='Editar'></a>&nbsp;&nbsp;&nbsp;<img style='width: 24px; height: 24px;' src=\"./img/png/garbage-2.png\" width=\"25%\" title='Eliminar' onclick='confirma_eliminar(".$aRow[$columnas[$i]].");'></img>";
            }
            else
            {
                $row[] = "";
            }
        }
        else if($columnas[$i] == "prenda")
        {
            $row[] = $aRow[$columnas[$i]];
        }
        else
        {
            $row[] = $aRow[$columnas[$i]];
        }
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
?>