<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_ingresos.php');
$model = new ingresos_model();
$rResult = $model->consulta_resumen_ingresos($_GET['fecha1'],$_GET['fecha2']) or die(mysql_error());

$columnas = array(
'fecha',
'valor',
'numero');

$output = array(
    "aaData" => array()
);

while ($aRow = mysql_fetch_array($rResult, MYSQL_ASSOC))
{
    $row = array();
    for ($i=0 ; $i<count($columnas); $i++)
    {
        if($aRow[$columnas[$i]]==null)
        {
            $row[]=0;
        }
        else
        {
            $row[] = $aRow[$columnas[$i]];
        }
    }
    $row[] = "<a href='reporte_abonos.php?fecha=".$aRow['fecha']."'><img  style='width: 24px; height: 24px;' src=\"./img/png/binoculars.png\" title='Ver'></a>";
    $output['aaData'][] = $row;
}
echo json_encode($output);
?>