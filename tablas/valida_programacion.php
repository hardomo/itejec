<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_programacion.php');
$model = new programacion_model();
$rResult = $model->consulta_detalles_validar_programacion($_GET['idprog']) or die(mysql_error());

$columnas = array(
'prenda',
'trabajo',
'complejidad',
'tiempo_estimado',
'fecha_entrega',
'url_foto',
'color',
'cliente',
'observaciones',
'estado',
'id_item');
    
$output = array(
    "aaData" => array()
);

while ($aRow = mysql_fetch_array($rResult, MYSQL_ASSOC))
{
    $row = array();
    for ($i=0 ; $i<count($columnas); $i++)
    {
        if($columnas[$i] == "id_item")
        {
            if($aRow['estado'] == "Pendiente")
            {
                $row[] = "<img style='width: 24px; height: 24px;' src=\"./img/png/minus.png\" title='Quitar' onclick='desprogramar_item(".$aRow[$columnas[$i]].")'>";
            }
            else if($aRow['estado'] == "En Proceso")
            {
                $row[] = "<img style='width: 24px; height: 24px;' src=\"./img/png/success.png\" title='En proceso'>";
            }
            else if($aRow['estado'] == "Terminado")
            {
                $row[] = "<img style='width: 24px; height: 24px;' src=\"./img/png/bandera-cuadros.png\" title='Terminado'>";
            }
            else
            {
                $row[] ="";
            }
        }
        else if($columnas[$i] == "url_foto" && strlen($aRow[$columnas[$i]])>0)
        {
            $row[] = "<button id=\"ver_foto\" type=\"button\" class=\"btn btn-secondary\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\" title=\"<img src='foto/img/".$aRow[$columnas[$i]]."'></img>\">Ver</button>";
        }
        else
        {
            $row[] = $aRow[$columnas[$i]];
        }
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
?>