<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_tipo_trabajo.php');
$model = new tipo_trabajo_model();
$rResult = $model->consulta_tipos_trabajo_rep() or die(mysql_error());

$columnas = array(
'nombre',
'descripcion',
'valor',
'tiempo',
'id');
    
$output = array(
    "aaData" => array()
);
    
while ($aRow = mysql_fetch_array($rResult, MYSQL_ASSOC))
{
    $row = array();
    for ($i=0 ; $i<count($columnas); $i++)
    {
        if($columnas[$i] == "id")
        {
            $row[] = "<a href=\"registro_tipo_trabajo.php?idtt=".$aRow[$columnas[$i]]."&accion=1\"><img style='width: 24px; height: 24px;' src=\"./img/png/edit.png\" title='Editar'></a>&nbsp;&nbsp;&nbsp;<img style='width: 24px; height: 24px;' src=\"./img/png/garbage-2.png\" width=\"25%\" title='Eliminar' onclick='confirma_eliminar(".$aRow[$columnas[$i]].");'></img>";
        }
        else
        {
            $row[] = $aRow[$columnas[$i]];
        }
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
?>