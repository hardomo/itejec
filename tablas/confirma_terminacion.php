<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_programacion.php');
$model = new programacion_model();
$rResult = $model->consulta_detalles_validar_programacion($_GET['idprog']) or die(mysql_error());

$columnas = array(
'prenda',
'trabajo',
'complejidad',
'tiempo_estimado',
'fecha_entrega',
'url_foto',
'color',
'cliente',
'observaciones',
'estado',
'id_item');
    
$output = array(
    "aaData" => array()
);

while ($aRow = mysql_fetch_array($rResult, MYSQL_ASSOC))
{
    $row = array();
    for ($i=0 ; $i<count($columnas); $i++)
    {
        if($columnas[$i] == "id_item")
        {
            $row[] ="<input type='checkbox' name='".$aRow[$columnas[$i]]."' id='".$aRow[$columnas[$i]]."' onchange=''>";
        }
        else if($columnas[$i] == "url_foto" && strlen($aRow[$columnas[$i]])>0)
        {
            $row[] = "<button id=\"ver_foto\" type=\"button\" class=\"btn btn-secondary\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\" title=\"<img src='foto/img/".$aRow[$columnas[$i]]."'></img>\">Ver</button>";
        }
        else
        {
            $row[] = $aRow[$columnas[$i]];
        }
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
?>