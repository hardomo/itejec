<?php
include 'sesion.php';
date_default_timezone_set('America/Bogota');
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/programacion.php');

$det_programacion = new programacion;
if(isset($_GET["fechaprog"]))
{
  $idprog = $det_programacion -> consulta_existe_programacion_fecha($_GET["fechaprog"]);
  if(!$idprog>0)
  {
    $idprog =0;
  }
}
else if (isset($_GET["idprog"]))
{
  $idprog = $_GET["idprog"];
}
else
{
  $idprog = 0;
}

$fecha = $det_programacion -> consulta_fecha_programacion($idprog);
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Asignación de prendas a contenedores</title>
 
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/rowgroup/1.1.0/css/rowGroup.dataTables.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script src=".\js\asigna_contenedor.js"></script>
  <script>
    var idprog= <?php echo $idprog;?>;
    $(document).ready(function()
    {
      oTable = $('#detalle_prog_validar').dataTable({
          "sDom": 'T<"clear">lfrtip',
          "bAutoWidth": false,
          "paging":   false,
          "bFilter": false,
          "bProcessing": true,
          "bServerSide": false,
          "aaSorting": [[ 0, "asc" ]],
          "bInfo": false,
          "sAjaxSource": "./tablas/asigna_contenedor.php?idprog=<?php echo $idprog;?>",
          "oLanguage":{
              "sProcessing":   "Procesando...",
              "sZeroRecords":  "No se encontraron resultados",
              "sSearch":       "Buscar:",
              "sUrl":          "",
              "sCopy":		 "Se han copiado los datos al portapapeles",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sPrevious": "Anterior",
                  "sNext":     "Siguiente",
                  "sLast":     "&Uacute;ltimo",
              }
          },
          "columnDefs": [
            {
              "targets": [8],
              "visible": false,
              "searchable": false
            }
          ],
          rowGroup: {
            startRender: function (rows,group) 
            {
              return $('<tr/>')
                .append('<td colspan="8">Orden de Trabajo '+group+'</td>' )
            },
            endRender: null,
            dataSrc: 8
          },
          "drawCallback": function(settings) {
            $('[data-toggle="tooltip"]').tooltip();
          }
      });
    });
  </script>
</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->


    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-2">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Asignación de Contenedores</h1>
              </div>
              <form class="user" name="form_programacion" id="form_programacion" rol="form" method="POST">
                <div class="form-group">
                    <h6>Programación del día:</h6>
                    <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Fecha" onchange="window.location.replace('asigna_contenedor.php?fechaprog='+$(this).val());" value=<?php echo $fecha;?>>
                    <span class="help-block" id="error"></span>
                </div>
              </form>
              <hr>
              <div class="table-responsive-sm">
                  <h6 class="m-0 font-weight-bold text-primary">Detalle de prendas a ubicar</h6>
                  <table class="table table-condensed table-sm" id="detalle_prog_validar" width="100%" cellspacing="0">
                      <thead>
                          <tr>
                            <th>Prenda</th>
                            <th>Trabajo</th>
                            <th>Fecha Entrega</th>
                            <th>Foto</th>
                            <th>Color</th>
                            <th>Marca</th>
                            <th>Observaciones</th>
                            <th>Contenedor</th>
                            <th>OT - Cliente</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                          <td colspan="9" class="dataTables_empty">---</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/rowgroup/1.1.0/js/dataTables.rowGroup.min.js"></script>
  <script> 
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#ubicar").addClass("active");
      });
    });
    </script> 
</body>
</html>