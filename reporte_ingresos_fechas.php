<?php
include 'sesion.php';
date_default_timezone_set('America/Bogota');
//require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/ingresos_fechas.php');
if(isset($_GET['fecha1']))
{
    $fecha1= $_GET['fecha1'];
}
else {
    $fecha1 = date("Y-m-d");
}
if(isset($_GET['fecha2']))
{
    $fecha2= $_GET['fecha2'];
}
else {
    $fecha2 = date("Y-m-d");
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Reporte de Ingresos</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.load('current', {'packages':['corechart']});
    </script>
  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script src=".\js\ingresos_fechas.js?v=1"></script>

</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->


    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Ingresos</h1>
              </div>
              <form class="user" name="form_ot" id="form_ot" rol="form" method="POST">
                <div class="container-fluid">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3"></div>
                        <form rol="form">
                            <div class="form-group row">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <input type="date" class="form-control" id="fecha1" name="fecha1" placeholder="Fecha Inicial" value=<?php echo $fecha1;?> onchange="recarga();"<?php echo $enable;?> onchange="recarga();">
                                    <span class="help-block" id="error"></span>
                                </div>
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <input type="date" class="form-control" id="fecha2" name="fecha2" placeholder="Fecha Final" value=<?php echo $fecha2;?> onchange="recarga();" <?php echo $enable;?>>
                                    <span class="help-block" id="error"></span>
                                </div>
                            </div>
                        </form>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-bordered" id="ingresos" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Fecha</th>
                              <th>Dinero Recibido</th>
                              <th>Núm. Órdenes</th>
                              <th>Opciones</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td colspan="4" class="dataTables_empty">---</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div id="columnchart_material" style = "width:100%; height: 500px;"></div>
                    </div>
                  </div>
                </div>
              </form>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script>
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#ingresos").addClass("active");
      });
    });
    </script>
</body>
</html>