<?php
include 'sesion.php';
date_default_timezone_set('America/Bogota');
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/orden_trabajo.php');
if(isset($_GET['fecha']))
{
    $fecha= $_GET['fecha'];
}
else {
    $fecha = date("Y-m-d");
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Órdenes de Trabajo por Fecha</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
    </script>

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <script src=".\js\ordenes_fecha.js"></script>
</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->

    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Órdenes de Trabajo Recibidas el día <?php echo $fecha;?></h1>
              </div>
                <input type=hidden id="fecha" value="<?php echo $fecha;?>"/>
                <div class="container-fluid">
                  <!-- DataTales Example -->
                  <div class="card shadow mb-4">
                    <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Órdenes de Trabajo</h6>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-bordered" id="detalle_ot" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Fecha Ingreso</th>
                              <th>Fecha Entrega</th>
                              <th>Cliente</th>
                              <th>Estado</th>
                              <th>Valor Total</th>
                              <th>Valor Abonos</th>
                              <th>Saldo Pendiente</th>
                              <th>Opciones</th>
                            </tr>
                          </thead>
                          <tfoot class="table-info">
                            <tr>
                              <td colspan="3">Totales</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                          </tfoot>
                          <tbody>
                            <tr>
                              <td colspan="7" class="dataTables_empty">---</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div id="chart_div"></div>
                    </div>
                  </div>
                </div>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!--<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>-->

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <!-- <script>
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#consultaOt").addClass("active");
      });
    });
    </script> -->
</body>
</html>