<?php
include 'sesion.php';
date_default_timezone_set('America/Bogota');
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/orden_trabajo.php');

$action = 0;
$MensajeAccion = "Agregar a la Orden de Trabajo";
$imprimir = "disabled=\"true\"";
if (isset($_GET["idot"]))
{
  $idot = $_GET["idot"];
}
else
{
  $idot = 0;
}

if (isset($_GET["iditem"]))
{
  $iditem = $_GET["iditem"];
  $action = 1;
  $MensajeAccion = "Guardar y volver a Orden de Trabajo";
  $imprimir = "";
}
else
{
  $iditem = 0;
}

$orden = new orden_trabajo;
$opciones = $orden->traer_opciones_prenda();

if($idot==0)
{
  $idot = $orden->crea_ot($_GET['cliente'],$_GET['fingreso'],$_GET['fentrega']);
}
if($iditem != 0)
{
  $detitem = new orden_trabajo;
  $item = $detitem->traer_datos_item($iditem);
  //var_dump($item);
  $infoticket = $detitem->traer_datos_etiqueta($iditem);
  //var_dump($infoticket);
  $idot = $item[0]['orden_trabajo'];
  $detalles = $detitem->traer_detalles($idot);
  //var_dump($detalles);
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registro de Prenda para Orden de Trabajo</title>
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <style>
		@media only screen and (max-width: 700px) 
		{
			video {
				max-width: 100%;
			}
		}
	</style>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- Required scripts -->
  <script type="text/javascript" src="js/dependencies/rsvp-3.1.0.min.js"></script>
  <script type="text/javascript" src="js/dependencies/sha-256.min.js"></script>
  <script type="text/javascript" src="js/qz-tray.js"></script>

   <script>
    <?php
      echo "iditem = ".$iditem.";";
      if($iditem !=0)
      { 
        echo "var tipo_prenda = ".$item[0]['tipo_prenda'].";";
        echo "var tipo_trabajo = ".$item[0]['tipo_trabajo'].";";
        echo "var complejidad = ".$item[0]['complejidad'].";";
        echo "var valor = ".$item[0]['valor'].";";
      }
    ?>
  </script>
  <script src=".\js\crea_prenda.js?v=3.2"></script>
</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->

    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Prenda</h1>
              </div>
              <form class="user" name="form_item" id="form_item" rol="form" action=".\controller\prenda_ot.php?idot=<?php echo $idot;?>&iditem=<?php echo $iditem;?>&action=<?php echo $action;?>" method="POST">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <select name="tipo_prenda" id="tipo_prenda" class="form-control" placeholder="Seleccione el tipo de prenda" onchange="select_tt($(this).val());">
                        <option selected disabled hidden>Seleccione tipo de prenda</option>
                        <?php echo $opciones['op_tipo_prenda'];?>
                    </select>
                    <span class="help-block" id="error"></span>
                  </div>
                  <div class="col-sm-6">
                    <select name="tipo_trabajo" id="tipo_trabajo" class="form-control" placeholder="Seleccione tipo de trabajo" onchange="calcular_valor(); $('#ttrabajo_p').html($(this).find('option:selected').text());">
                      <option selected disabled hidden>Seleccione tipo de trabajo</option>
                      <?php echo $opciones['op_tipo_trabajo'];?>
                    </select>
                    <span class="help-block" id="error"></span> 
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <select name="complejidad" id="complejidad" class="form-control" placeholder="Seleccione complejidad del trabajo" onchange="calcular_valor();">
                      <option selected disabled hidden>Seleccione complejidad del trabajo</option>
                      <?php echo $opciones['op_complejidad'];?>
                    </select>
                    <span class="help-block" id="error"></span>
                  </div>
                  <div class="col-sm-6">
                    <input type="button" class="form-control" id="tomar_foto" name="tomar_foto" value="Tomar Foto" onclick="$('#camara').show();">
                    <input type="text" class="form-control" id="url_foto" name="url_foto" value=<?php echo $item[0]['url_foto'];?>>
                    <button id="ver_foto" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<img src='foto/img/<?php echo $item[0]['url_foto'];?>'></img>">Ver Foto</button>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <div id="camara" style="display:none;">
                      <h6>Seleccione la cámara</h6>
                      <div>
                        <select name="listaDeDispositivos" id="listaDeDispositivos"></select>
                        <a id="boton" class="btn btn-warning">Tomar captura</a>
                        <p id="estado"></p>
                      </div>
                      <video muted="muted" id="video"></video>
                      <canvas id="canvas" style="display: none;"></canvas>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control" id="talla" name="talla" placeholder="Talla" value="<?php echo $item[0]['talla'];?>">
                    <span class="help-block" id="error"></span>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="color" name="color" placeholder="Color" value="<?php echo $item[0]['color'];?>">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control" id="marca" name="marca" placeholder="Marca" value="<?php echo $item[0]['marca'];?>">
                    <span class="help-block" id="error"></span>
                  </div>
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="number" class="form-control" id="valor" name="valor" readonly="readonly" placeholder="Valor $">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" id="observaciones" name="observaciones" placeholder="Observaciones" onblur="$('#obs_p').html($(this).val());"><?php echo $item[0]['observaciones'];?></textarea>
                  <span class="help-block" id="error"></span>
                </div>
              <button type="submit" class="btn btn-primary btn-user btn-block"><?php echo $MensajeAccion;?></button>
                
              </form>
              <hr>
              <button type="submit" <?php echo $imprimir;?> class="btn btn-primary btn-user btn-block" onclick="printData();">Imprimir etiqueta</button>
    
              <!--<div class="form-group row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary btn-user btn-block">Agregar a la Orden</button>
                  </div>
              </form>
                  <div class="col-sm-6">
                    <button class="btn btn-primary btn-user btn-block" onclick="printData();">Imprimir etiqueta</button>
                  </div>
              </div>-->
              <div id="ticket" class="form-group" style="display:none;">
                <span id="Pidot_p"><?php echo $item[0]['orden_trabajo'];?></span>
                <span id="Ptprenda_p"><?php echo $infoticket[0]['prenda'];?></span>
                <span id="Pttrabajo_p"><?php echo $infoticket[0]['tipo_trabajo'];?></span>
                <span id="Pobs_p"><?php echo $item[0]['observaciones'];?></span>
                <span id="Pcolor_p"><?php echo $item[0]['color'];?></span>
                <span id="Pfecha_ingreso_p"><?php echo $detalles[0]['fecha_ingreso'];?></span>
                <span id="Pfecha_entrega_p"><?php echo $detalles[0]['fecha_entrega'];?></span>
                <span id="Pestado_ot_p"><?php echo $detalles[0]['estado_ot'];?></span>
                <span id="Pnombre_p"><?php echo $detalles[0]['nombre'];?></span>
                <span id="Pcedula_p"><?php echo $detalles[0]['cedula'];?></span>
                <span id="Pdireccion_p"><?php echo $detalles[0]['direccion'];?></span>
                <span id="Ptelefono1_p"><?php echo $detalles[0]['telefono1'];?></span>
                <span id="Pmarca_p"><?php echo $infoticket[0]['marca'];?></span>
              </div>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
  <script> 
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#tipoPrenda").addClass("active");
      });
      
    });
    </script> 

<script src=".\js\foto.js"></script>
</body>
</html>