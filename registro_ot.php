<?php
include 'sesion.php';
date_default_timezone_set('America/Bogota');
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/controller/orden_trabajo.php');

if (isset($_GET["idot"]))
{
  $idot = $_GET["idot"];
  $enable = "disabled";
}
else
{
  $idot = 0;
  $enable = "";
}

if (isset($_GET["idcliente"]))
{
  $idcliente = $_GET["idcliente"];
  $printEnable = "disabled";
}
else
{
  $idcliente = 0;
}

$det_orden = new orden_trabajo;
$detalles = $det_orden->traer_detalles($idot);
$items = $det_orden->traer_items($idot);
$estado_cuenta = $det_orden->traer_estado_cuenta($idot);

if($detalles[0]['estado_ot'] == "Entregado")
{
  $entregaEnable = "disabled";
}
else
{
  $entregaEnable = "";
}

if($estado_cuenta['saldo']<=0)
{
  $abonoEnable = "disabled";
}
else
{
  $abonoEnable = "";
}

if($idcliente>0)
{
  $op_clientes = $det_orden -> traer_op_clientes($idcliente);
}
else
{
  $op_clientes = $det_orden -> traer_op_clientes($detalles[0]['id_cliente']);
}

if($idot == 0)
{
  $detalles[0]['fecha_ingreso'] = date("Y-m-d");
}
if($idot >0)
{
  if($detalles[0]['estado_ot'] == "Pendiente")
  {
    $disp_confirmar = "";
    $printEnable = "disabled";
    $en_ap = "";
  }
  else
  {
    $disp_confirmar = "disabled";
    $en_ap = "disabled";
    $printEnable = "";
  }
}
if($detalles[0]['idprog']>0)
{
  $idprog = $detalles[0]['idprog'];
}
else
{
  $idprog = 0;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registro de Órdenes de Trabajo</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Required scripts -->
    <script type="text/javascript" src="js/dependencies/rsvp-3.1.0.min.js"></script>
    <script type="text/javascript" src="js/dependencies/sha-256.min.js"></script>
    <script type="text/javascript" src="js/qz-tray.js"></script>

  <script src=".\js\crea_ot.js?v=1.7"></script>
  <script>
    var idot = <?php echo $idot;?>;
    var idcliente = <?php echo $idcliente;?>;

    $(document).ready(function()
    {
      saldoPendiente = <?php echo $estado_cuenta['saldo'];?>;
      oTable = $('#detalle_ot').dataTable({
          "sDom": 'T<"clear">lfrtip',
          "bAutoWidth": false,
          "paging":   false,
          "bFilter": false,
          "bProcessing": true,
          "bServerSide": false,
          "aaSorting": [[ 0, "asc" ]],
          "bInfo": false,
          "sAjaxSource": "./tablas/orden_trabajo.php?idot=<?php echo $idot;?>",
          "oLanguage":{
              "sProcessing":   "Procesando...",
              "sZeroRecords":  "No se encontraron resultados",
              "sSearch":       "Buscar:",
              "sUrl":          "",
              "sCopy":		 "Se han copiado los datos al portapapeles"
          }
      });

      oTable2 = $('#tabla_abonos').dataTable({
          "sDom": 'T<"clear">lfrtip',
          "bAutoWidth": false,
          "paging":   false,
          "bFilter": false,
          "bProcessing": true,
          "bServerSide": false,
          "aaSorting": [[ 0, "asc" ]],
          "bInfo": false,
          "sAjaxSource": "./tablas/abonos_ot.php?idot=<?php echo $idot;?>",
          "oLanguage":{
              "sProcessing":   "Procesando...",
              "sZeroRecords":  "No se encontraron resultados",
              "sSearch":       "Buscar:",
              "sUrl":          "",
              "sCopy":		 "Se han copiado los datos al portapapeles"
          },
        "footerCallback": function ( row, data, start, end, display )
        {
            var api = this.api();
            var j = 1;
            var pageTotal = api
            .column( j, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return Number(a) + Number(b);
            }, 0 );
            $( api.column(j).footer()).html(pageTotal);
        }
      });

      if(idcliente>0)
      {
        $("#cliente").change();
      }
  });
  </script>
</head>

<body class="bg-gradient-primary">

<div id="wrapper">

<!-- Incluir menu lateral-->
<div id="includedMenu"></div>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Incluir menu superior-->

    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Orden de Trabajo&nbsp;<span><?php if($idot>0){echo $idot;}?></span></h1>
              </div>
              <form class="user" name="form_ot" id="form_ot" rol="form" method="POST">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Fecha de Ingreso" value=<?php echo $detalles[0]['fecha_ingreso'];?> <?php echo $enable;?>>
                    <span class="help-block" id="error"></span>
                  </div>
                  <div class="col-sm-6">
                    <select name="cliente" id="cliente" class="form-control" placeholder="Seleccione cliente" onchange="trae_detalles_cliente($(this).val());" <?php echo $enable;?>>
                        <?php echo $op_clientes; ?>
                    </select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                <div class="form-group">
                  <h6 class="m-0 font-weight-bold text-primary">Detalles del Cliente:</h6>
                  Nombre: <span class="" id="nombre_cliente"><?php echo $detalles[0]['nombre'];?></span><br>
                  Cédula: <span class="" id="cedula"><?php echo $detalles[0]['cedula'];?></span><br>
                  Correo Electrónico: <span class="" id="correo_e"><?php echo $detalles[0]['correo_e'];?></span><br>
                  Dirección: <span class="" id="direccion"><?php echo $detalles[0]['direccion'];?></span><br>
                  Teléfono: <span class="" id="telefono1"><?php echo $detalles[0]['telefono1'];?></span><br>
                  Teléfono 2: <span class="" id="telefono2"><?php echo $detalles[0]['telefono2'];?></span><br>
                  Fecha de Nacimiento: <span class="" id="fecha_nac"><?php echo $detalles[0]['fecha_nacimiento'];?></span><br>
                </div>
                <div class="form-group row">
                  <div class="col-sm-2 mb-3 mb-sm-0">
                    <h6 class="m-0 font-weight-bold text-primary">Estado de la Orden:</h6> <span class="" id="estado_ot"><?php echo $detalles[0]['estado_ot'];?></span>
                  </div>
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <a id="b_confirmaot" class="btn btn-danger <?php echo $disp_confirmar;?>" style="color:white;" onclick="confirma_ot(<?php echo $idot;?>);">Confirmar Orden</a>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-2 mb-3 mb-sm-0">
                    <h6 class="m-0 font-weight-bold text-primary">Fecha de Entrega:</h6>
                  </div>
                  <div class="col-sm-5">
                    <input type="date" class="form-control" id="fecha_entrega" name="fecha_entrega" placeholder="Fecha de Entrega" onchange="actualizar_f_entrega($(this).val());" value=<?php echo $detalles[0]['fecha_entrega'];?>>
                    <span class="help-block" id="error"></span>
                    </form>
                  </div>
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <a id="ver_prog" href="registro_programacion.php?idprog=<?php echo $idprog;?>" target="_blank" onclick="window.open(this.href, this.target, 'width=800,height=500,resizable=yes,scrollbars=yes'); return false;">Ver programación</a>
                  </div>
                </div>

                <!--<div class="container-fluid">-->
                  <div class="card shadow mb-4">
                    <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Detalle de la Orden de Trabajo</h6>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-bordered" id="detalle_ot" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Prenda</th>
                              <th>Marca</th>
                              <th>Trabajo</th>
                              <th>Valor</th>
                              <th>Programación</th>
                              <th>Contenedor</th>
                              <th>Estado</th>
                              <th>Opciones</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td colspan="7" class="dataTables_empty">---</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                <!--</div>-->
                <div class="form-group">
                  <h6 class="m-0 font-weight-bold text-primary">Estado de Cuenta:</h6>
                  Valor total de la orden: $<span class="" id="nombre_cliente"><?php echo $estado_cuenta['total'];?></span><br>
                </div>
                </form>
                <div class="table-responsive">
                  <h6 class="m-0 font-weight-bold text-primary">Abonos:</h6>
                  <table class="table table-bordered" id="tabla_abonos" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Valor</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tfoot class="table-info">
                      <tr>
                        <td>Total</td>
                        <td></td>
                        <td></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr>
                        <td colspan="3" class="dataTables_empty"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <h6 class="m-0 font-weight-bold text-primary" >Saldo pendiente: </h6> $<span class="" id="saldopendiente"><?php echo $estado_cuenta['saldo'];?></span><br>
                <div class="form-group row">
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <a id="bap" class="btn btn-primary btn-user btn-block  <?php echo $en_ap;?>" onclick="preparar_ot(<?php echo $idot;?>);" style="color:white;">Agregar prenda</a>
                  </div>
                  <div class="col-sm-3">
                    <button id="babono" class="btn btn-primary btn-user btn-block" <?php echo $abonoEnable;?> onclick="$('#dialog2').modal('show');">Registrar abono</button>
                  </div>
                  <div class="col-sm-3">
                    <button id="bentrega" class="btn btn-primary btn-user btn-block" onclick="entrega_ot(<?php echo $idot;?>)" <?php echo $entregaEnable;?>>Confirmar Entrega</button>
                  </div>
                  <div class="col-sm-3">
                    <button class="btn btn-primary btn-user btn-block" <?php echo $printEnable;?> onclick="printData()">Imprimir</button>
                  </div>
                </div>

              <input type="hidden" id="iditemborrar" style:"display:none;">
              <div id="dialog" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Confirme Eliminación</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>¿En realidad desea eliminar este item de la orden de trabajo?</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" onclick="elimina_item($('#iditemborrar').val());">Sí</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div id="dialog2" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Abono</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form class="user" name="form_abono" id="form_abono" rol="form" action=".\controller\abono_ot.php" method="GET">
                        <input class="form-control" id="idabonoeditar" name="idabonoeditar" type="hidden">
                        <input class="form-control" id="action" name="action" type="hidden">
                        <input class="form-control" id="idot" name="idot" type="hidden" value=<?php echo $idot;?>>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="date" class="form-control" id="fechaabono" name="fechaabono" placeholder="Fecha de Ingreso" onchange="$('#fecha_p').html($(this).val());">
                                <span class="help-block" id="error"></span>
                            </div>
                            <div class="col-sm-6">
                                <input type="number" class="form-control" id="valorabono" name="valorabono" placeholder="Valor $"  onblur="$('#valor_p').html($(this).val());">
                                <span class="help-block" id="error"></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-user btn-block">Guardar</button>
                      </form>
                      <div id="tiquete" style="display:none;">

                      <span id="Pitems_imprimir">

                        <?php
                          echo "\n";
                          echo "*************************\n";
                          foreach ($items as $item){
                            echo $item['prenda']." ".$item['color']."\n";
                            echo $item['tipo_trabajo']."\n";
                            echo "Valor: ".$item['valor']."\n";
                            echo "*************************\n";
                          }
                        ?>
                      </span>
                      <span id="Pestado_ot"><?php echo $detalles[0]['estado_ot']; ?></span>
                      <span id="Pfecha_orden"><?php echo $detalles[0]['fecha_ingreso']; ?></span>
                      <span id="Pfecha_entrega"><?php echo $detalles[0]['fecha_entrega']; ?></span>
                      <span id="Pnombrecliente"><?php echo $detalles[0]['nombre']; ?></span>
                      <span id="Pcedula"><?php echo $detalles[0]['cedula']; ?></span>
                      <span id="Pdireccion"><?php echo $detalles[0]['direccion']; ?></span>
                      <span id="Ptotal"><?php echo $estado_cuenta['total']; ?></span>
                      <span id="Pabonos"><?php echo $estado_cuenta['abonos']; ?></span>
                      <span id="Psaldo"><?php echo $estado_cuenta['saldo']; ?></span>
                      <span id="Pidot_p"><?php echo $idot; ?></span>
                      <span id="Pfecha_actual"><?php echo $detalles[0]['fecha_ingreso']; ?></span>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <span></span>
                    </div>
                  </div>
                </div>
              </div>

              <div id="dialog3" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Prendas Entregadas</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>Las prendas han sido entregadas al cliente.</p>
                    </div>
                    <div class="modal-footer">
                    </div>
                  </div>
                </div>
              </div>

              <div id="dialog4" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Alerta</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>Antes de confirmar la orden debe agregar las prendas y la fecha de entrega</p>
                    </div>
                    <div class="modal-footer">
                    </div>
                  </div>
                </div>
              </div>

              <div id="dialog5" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Información</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>Aún no hay programación para esta fecha. Se creará en caso de ser necesario.</p>
                    </div>
                    <div class="modal-footer">
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script>
    $(function(){
      $( "#includedMenu" ).load( "menu.php", function() {
        $("#nuevaOt").addClass("active");
      });
    });
    </script>
</body>
</html>