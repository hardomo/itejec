<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_programacion.php');

class programacion
{
    private $model;

    public function __construct()
    {
        $this-> model = new programacion_model();
    }
    
    public function traer_detalles_programacion($idprog)
    {
        $consulta = $this->model->consulta_detalles_programacion($idprog);
        return $consulta;
    }

    public function consulta_fecha_programacion($idprog)
    {
        $consulta = $this->model->consulta_fecha_programacion($idprog);
        return $consulta[0]['fecha'];
    }

    public function consulta_items_pendientes_programacion($fechamax)
    {
        $consulta = $this->model->consulta_items_pendientes_programacion($fechamax);
        return $consulta;
    }

    public function consulta_tiempo_total_programacion($idprog)
    {
        $consulta = $this->model->consulta_tiempo_total_programacion($idprog);
        return $consulta[0]['tiempo_total'];
    }

    public function consulta_existe_programacion_fecha($fecha)
    {
        $consulta = $this->model->consulta_existe_programacion_fecha($fecha);
        return $consulta[0]['id'];
    }
}
?>