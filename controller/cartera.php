<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_cartera.php');

class cartera
{
    private $model;

    public function __construct()
    {
        $this-> model = new cartera_model();
    }

    public function traer_estado_cartera($fecha1,$fecha2)
    {
        $consulta = $this->model->consulta_resumen_cartera($fecha1,$fecha2);
        return $consulta;
    }
}

?>