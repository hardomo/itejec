<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_complejidad.php');

class complejidad
{
    private $model;

    public function __construct()
    {
        $this-> model = new complejidad_model();
    }
    
    public function traer_datos_comp($idcomp)
    {
        $consulta = $this->model->consulta_comp_id($idcomp);
        return $consulta;
    }
}
?>