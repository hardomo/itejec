<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_programacion.php');

$model = new programacion_model();
$existe = $model -> consulta_existe_programacion_fecha($_POST['fecha']);
if($existe[0]['id'] != 0)
{
    header("Location: ../registro_programacion.php?idprog=".$existe[0]['id']);
    die();
}
else
{
    $consulta = $model->crea_programacion($_POST['fecha']);
    $existe = $model -> consulta_existe_programacion_fecha($_POST['fecha']);
    header("Location: ../registro_programacion.php?idprog=".$existe[0][id]);
    die();
}
?>