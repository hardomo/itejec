<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_clientes.php');

$model = new clientes_model();

if($_GET['action'] == '1')
{
    $consulta = $model->edita_cliente($_GET['idcliente'],$_POST['nombre'],$_POST['cedula'],$_POST['correo'],$_POST['direccion'],$_POST['telefono1'],$_POST['telefono2'],$_POST['fecha_nac']);
}
else
{
    $consulta = $model->crea_cliente($_POST['nombre'],$_POST['cedula'],$_POST['correo'],$_POST['direccion'],$_POST['telefono1'],$_POST['telefono2'],$_POST['fecha_nac']);
}

if($consulta ==1)
{
    $idcliente = $model->consulta_id_cliente_x_cedula($_POST['cedula']);
    header("Location: ../consulta_ot_cliente.php?idcliente=".$idcliente[0]);
    die();
}
else if($consulta == 0)
{

    header("Location: ../registro_cliente.php?exito=0");
    die();
}
?>