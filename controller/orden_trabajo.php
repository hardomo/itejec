<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_ot.php');

class orden_trabajo
{
    private $model;

    public function __construct()
    {
        $this-> model = new ot_model();
    }

    public function traer_detalles($idot)
    {
        $consulta = $this->model->consulta_detalles_cliente_ot($idot);
        return $consulta;
    }

    public function traer_items($idot)
    {
        $consulta = $this->model->consulta_items_ot_id_array($idot);
        return $consulta;
    }

    public function traer_estado_cuenta($idot)
    {
        $estado_cuenta['abonos'] = $this->model->consulta_abonos_ot($idot);
        $estado_cuenta['total'] = $this->model->consulta_valor_total_ot($idot);
        $estado_cuenta['saldo'] = $estado_cuenta['total']-$estado_cuenta['abonos'];
        return $estado_cuenta;
    }

    public function traer_opciones_prenda()
    {
        $opciones_prenda['complejidad']=$this->model->consulta_complejidad();
        $opciones_prenda['tipo_prenda']=$this->model->consulta_tipo_prenda();
        $opciones_prenda['tipo_trabajo']=$this->model->consulta_tipo_trabajo($iditem);

        $op_complejidad = "";
        for($i=0; $i<count($opciones_prenda['complejidad']); $i++)
        {
            $op_complejidad.="<option value=".$opciones_prenda['complejidad'][$i]['id']." porc=".$opciones_prenda['complejidad'][$i]['incremento_porcentual'].">".$opciones_prenda['complejidad'][$i]['nombre']." (+".$opciones_prenda['complejidad'][$i]['incremento_porcentual']."%)</option>";
        }
        $opciones['op_complejidad']=$op_complejidad;

        $op_tipo_prenda = "";
        for($i=0; $i<count($opciones_prenda['tipo_prenda']); $i++)
        {
            $op_tipo_prenda.="<option value=".$opciones_prenda['tipo_prenda'][$i]['id'].">".$opciones_prenda['tipo_prenda'][$i]['nombre']."</option>";
        }
        $opciones['op_tipo_prenda']=$op_tipo_prenda;

        $op_tipo_trabajo = "";
        for($i=0; $i<count($opciones_prenda['tipo_trabajo']); $i++)
        {
            $op_tipo_trabajo.="<option value=".$opciones_prenda['tipo_trabajo'][$i]['id']." precio=".$opciones_prenda['tipo_trabajo'][$i]['valor'].">".$opciones_prenda['tipo_trabajo'][$i]['nombre']."</option>";
        }
        $opciones['op_tipo_trabajo']=$op_tipo_trabajo;

        return $opciones;
    }

    public function traer_op_clientes($idcliente)
    {
        $clientes=$this->model->consulta_clientes_select();

        if(isset($idcliente) && $idcliente != 0)
        {
            $op_clientes = "";
        }
        else
        {
            $op_clientes = "<option selected disabled>Cliente</option>";
        }

        for($i=0; $i<count($clientes); $i++)
        {
            if($clientes[$i]['id'] == $idcliente)
            {
                $op_clientes.="<option value=".$clientes[$i]['id']." SELECTED>".$clientes[$i]['nombre']."</option>";
            }
            else
            {
                $op_clientes.="<option value=".$clientes[$i]['id'].">".$clientes[$i]['nombre']."</option>";
            }
        }
        return $op_clientes;
    }

    public function crea_ot($cliente,$fingreso,$fentrega)
    {
        $ot = $this->model->crea_ot($cliente,$fingreso,$fentrega);
        if($ot == 1)
        {
            $idot = $this->model->consulta_ultima_ot_cliente($cliente);
            return $idot[0]['id'];
        }
        else
        return 0;
    }

    public function traer_detalles_cliente($idcliente)
    {
        $consulta = $this->model->consulta_cliente_id($idcliente);
        return $consulta;
    }

    public function traer_ordenes_cliente($idcliente)
    {
        $consulta = $this->model->consulta_ordenes_cliente($idcliente);
        return $consulta;
    }

    public function traer_datos_item($iditem)
    {
        $consulta = $this->model->consulta_detalles_item($iditem);
        return $consulta;
    }

    public function traer_datos_etiqueta($iditem)
    {
        $consulta = $this->model->consulta_items_etiqueta($iditem);
        return $consulta;
    }

    public function traer_ordenes_fecha($fecha)
    {
        $clientes=$this->model->consulta_clientes_select();

        for($i=0; $i<count($clientes); $i++)
        {
            if($clientes[$i]['id'] == $idcliente)
            {
                $op_clientes.="<option value=".$clientes[$i]['id']." SELECTED>".$clientes[$i]['nombre']."</option>";
            }
            else
            {
                $op_clientes.="<option value=".$clientes[$i]['id'].">".$clientes[$i]['nombre']."</option>";
            }
        }
        return $op_clientes;
    }

}
?>