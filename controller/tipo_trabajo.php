<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_tipo_trabajo.php');

class tipo_trabajo
{
    private $model;

    public function __construct()
    {
        $this-> model = new tipo_trabajo_model();
    }
    
    public function traer_datos_tt($idtt)
    {
        $consulta = $this->model->consulta_tt_id($idtt);
        return $consulta;
    }
}
?>