<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_usuarios.php');
$model = new usuarios_model();
if($_GET['action']==0)
{
    $consulta = $model->crea_usuario($_POST['nombre'],$_POST['nombre_usuario'],$_POST['cedula'],$_POST['correo'],$_POST['direccion'],$_POST['telefono1'],$_POST['telefono2'],$_POST['password'],$_POST['rol']);
}
else if($_GET['action']==1)
{
    $consulta = $model->edita_usuario($_GET['idusuario'],$_POST['nombre'],$_POST['nombre_usuario'],$_POST['cedula'],$_POST['correo'],$_POST['direccion'],$_POST['telefono1'],$_POST['telefono2'],$_POST['password'],$_POST['rol']);
}
else if($_GET['action']==2)
{
    $consulta = $model->elimina_usuario($_GET['idusuarioborrar']);
}

if($consulta == 1)
{
    if($_GET['action'] == 0)
    {
        header("Location: ../registro_usuario.php?exito=1");
    }
    else if($_GET['action'] == 1)
    {
        header("Location: ../reporte_usuarios.php");
    }
    die();
}
else if($consulta == 0 && $_GET['action'] != 2)
{
    header("Location: ../registro_usuario.php?exito=0");
    die();
}
?>