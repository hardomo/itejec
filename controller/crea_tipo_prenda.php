<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_tipo_prenda.php');

$model = new tipo_prenda_model();

if($_GET['action']==0)
{
    $consulta = $model->crea_tipo_prenda($_POST['nombre']);
}
else if($_GET['action']==1)
{
    $consulta = $model->edita_tipo_prenda($_GET['idtp'],$_POST['nombre']);
    //echo $consulta;die();
}
else if($_GET['action']==2)
{
    $consulta = $model->elimina_tipo_prenda($_GET['idtpborrar']);
}

if($consulta ==1)
{
    if($_GET['action'] == 0)
    {
        header("Location: ../registro_tipo_prenda.php?exito=1");
    }
    else if($_GET['action'] == 1)
    {
        header("Location: ../reporte_tipos_prenda.php?");
    }
    die();
}
else if($consulta == 0 && $_GET['action'] == 0)
{
    header("Location: ../registro_tipo_prenda.php?exito=0");
    die();
}
else if($consulta == 0 && $_GET['action'] == 2)
{
    //header("Location: ../reporte_contenedores.php?exito=0");
    echo ("./reporte_tipos_prenda.php?exito=0");
    die();
}
?>