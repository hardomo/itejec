<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_tipo_prenda.php');

class tipo_prenda
{
    private $model;

    public function __construct()
    {
        $this-> model = new tipo_prenda_model();
    }
    
    public function traer_datos_tp($idtp)
    {
        $consulta = $this->model->consulta_tp_id($idtp);
        return $consulta;
    }
}
?>