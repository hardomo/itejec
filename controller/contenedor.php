<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_contenedores.php');

class contenedor
{
    private $model;

    public function __construct()
    {
        $this-> model = new contenedor_model();
    }
    
    public function traer_datos_cont($idcontenedor)
    {
        $consulta = $this->model->consulta_contenedor_id($idcontenedor);
        return $consulta;
    }
}
?>