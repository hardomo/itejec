<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_contenedores.php');

$model = new contenedor_model();

if($_GET['action']==0)
{
    $consulta = $model->crea_contenedor($_POST['nombre']);
}
else if($_GET['action']==1)
{
    $consulta = $model->edita_contenedor($_GET['idcont'],$_POST['nombre']);
}
else if($_GET['action']==2)
{
    $consulta = $model->elimina_contenedor($_GET['idcontborrar']);
}

if($consulta ==1)
{
    if($_GET['action'] == 0)
    {
        header("Location: ../reporte_contenedores.php");
    }
    else if($_GET['action'] == 1)
    {
        header("Location: ../reporte_contenedores.php");
    }
    die();
}
else if($consulta == 0 && $_GET['action'] == 0)
{
    header("Location: ../registro_contenedor.php?exito=0");
    die();
}
else if($consulta == 0 && $_GET['action'] == 2)
{
    //header("Location: ../reporte_contenedores.php?exito=0");
    echo ("./reporte_contenedores.php?exito=0");
    die();
}
?>