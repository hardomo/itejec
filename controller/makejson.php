<?php
header("Content-type: application/json; charset=utf-8");
function make_json($item) 
{
    foreach($item as $key => $value)
    {
        if(is_array($value))
        {
            $arr[] = '"'.$key.'":'.make_json($item[$key]);
        } 
        else 
        {
            $arr[] = '"'.$key.'":"'.str_replace(array("\\", "\""), array("\\\\", "\\\""), $value).'"';
        }
    }
    return '{'.implode(",",$arr)."}";
}
?>