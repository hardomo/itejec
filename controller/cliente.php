<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_clientes.php');

class cliente
{
    private $model;

    public function __construct()
    {
        $this-> model = new clientes_model();
    }
    
    public function traer_detalles($idcliente)
    {
        $consulta = $this->model->consulta_cliente_id($idcliente);
        return $consulta;
    }

    public function consulta_clientes()
    {
        $consulta = $this->model->consulta_clientes();
        return $consulta;
    }
}
?>