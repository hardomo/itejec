<?php
//require_once("../bd/consultas_clientes.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_tipo_trabajo.php');
$model = new tipo_trabajo_model();

if($_GET['action']==0)
{
    $consulta = $model->crea_tipo_trabajo($_POST['nombre'],$_POST['descripcion'],$_POST['valor'],$_POST['tiempo']);
}
else if($_GET['action']==1)
{
    $consulta = $model->edita_tipo_trabajo($_GET['idtt'],$_POST['nombre'],$_POST['descripcion'],$_POST['valor'],$_POST['tiempo']);
}
else if($_GET['action']==2)
{
    $consulta = $model->elimina_tipo_trabajo($_GET['idttborrar']);
}


if($consulta ==1)
{
    if($_GET['action'] == 0)
    {
        header("Location: ../registro_tipo_trabajo.php?exito=1");
    }
    else if($_GET['action'] == 1)
    {
        header("Location: ../reporte_tipos_trabajo.php");
    }
    die();
}
else if($consulta == 0 && $_GET['action'] == 0)
{
    header("Location: ../registro_tipo_trabajo.php?exito=0");
    die();
}
else if($consulta == 0 && $_GET['action'] == 2)
{
    //header("Location: ../reporte_contenedores.php?exito=0");
    echo ("./reporte_tipos_trabajo.php?exito=0");
    die();
}
?>