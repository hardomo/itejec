<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_ingresos.php');

class ingresos
{
    private $model;

    public function __construct()
    {
        $this-> model = new ingresos_model();
    }

    public function traer_ingresos($fecha1,$fecha2)
    {
        $consulta = $this->model->consulta_resumen_ingresos($fecha1,$fecha2);
        return $consulta;
    }

    public function traer_abonos_fecha($fecha)
    {
        $consulta = $this->model->consulta_abonos_fecha($fecha);
        return $consulta;
    }
}

?>