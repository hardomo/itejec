<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_complejidad.php');
$model = new complejidad_model();

if($_GET['action']==0)
{
    $consulta = $model->crea_complejidad($_POST['nombre'],$_POST['incremento_porcentual'],$_POST['incremento_tiempo']);
}
else if($_GET['action']==1)
{
    $consulta = $model->edita_complejidad($_GET['idcomp'],$_POST['nombre'],$_POST['incremento_porcentual'],$_POST['incremento_tiempo']);
}
else if($_GET['action']==2)
{
    $consulta = $model->elimina_complejidad($_GET['idcompborrar']);
}


if($consulta ==1)
{
    if($_GET['action'] == 0)
    {
        header("Location: ../reporte_complejidades.php");
    }
    else if($_GET['action'] == 1)
    {
        header("Location: ../reporte_complejidades.php");
    }
    die();
}
else if($consulta == 0 && $_GET['action'] == 0)
{
    header("Location: ../registro_complejidad.php?exito=0");
    die();
}
else if($consulta == 0 && $_GET['action'] == 2)
{
    echo ("./reporte_complejidades.php?exito=0");
    die();
}
?>