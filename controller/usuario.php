<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/itejec/bd/consultas_usuarios.php');

class usuario
{
    private $model;

    public function __construct()
    {
        $this-> model = new usuarios_model();
    }
    
    public function traer_datos_usuario($idusuario)
    {
        $consulta = $this->model->consulta_usuario_id($idusuario);
        return $consulta;
    }

    public function traer_opciones_rol()
    {
        $consulta = $this->model->consulta_opciones_rol();
        return $consulta;
    }
}
?>