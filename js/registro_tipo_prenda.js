$('document').ready(function()
{ 
    var exito = getParameterByName('exito');
    if (exito == "1")
    {
        $("#exito").show();
        setTimeout("$('#exito').hide();",3000);
    }
    else if(exito == "0")
    {
        $("#falla").show();
        setTimeout("$('#falla').hide();",3000);
    }
});

function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}