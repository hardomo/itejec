var oTable;
$('document').ready(function() {
    oTable = $('#clientes').dataTable({
        "aaSorting": [
            [0, "asc"]
        ],
        "bInfo": false,
        "sAjaxSource": "./tablas/clientes.php",
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sCopy": "Se han copiado los datos al portapapeles",
            "oPaginate": {
                "sFirst": "Primero",
                "sPrevious": "Anterior",
                "sNext": "Siguiente",
                "sLast": "&Uacute;ltimo",
            }
        }
    });
});