function termina_seleccion()
{
    oTable.fnFilterClear();
    t_asignar = new Array();
    $("input[type=checkbox]:checked").each(function()
    {
      t_asignar.push($(this).attr('id'));
    });

    t_desasignar = new Array();
    $("input[type=checkbox]:unchecked").each(function()
    {
      t_desasignar.push($(this).attr('id'));
    });

    $.ajax(
    {
        //data: {t_asignar:t_asignar, t_desasignar:t_desasignar, tprenda:idtp},
        data: {t_asignar:t_asignar, tprenda:idtp},
        url: './controller/asigna_trabajos.php',
        success: function(respuesta)
        {
            $('#dialog5').modal('show');
            setTimeout(function(){$('#dialog5').modal('hide');}, 3000);
        },
        error: function (request, status, error) 
        {}
    });
}