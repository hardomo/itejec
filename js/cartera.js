var oTable;
$('document').ready(function() {

    oTable = $('#cartera').DataTable({
        "aaSorting": [
            [0, "asc"]
        ],
        "bInfo": false,
        "sAjaxSource": "./tablas/cartera.php?fecha1=" + $("#fecha1").val() + "&fecha2=" + $("#fecha2").val(),
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sCopy": "Se han copiado los datos al portapapeles",
            "oPaginate": {
                "sFirst": "Primero",
                "sPrevious": "Anterior",
                "sNext": "Siguiente",
                "sLast": "&Uacute;ltimo",
            }
        },
        "initComplete": function() {
            google.charts.setOnLoadCallback(function() {

                var data = [];
                if (oTable) {
                    var rows = oTable.rows();
                    data = rows.data();
                } else {
                    console.log(this.IDENTITY + " -> getData() table undefined " + table);
                }

                totalabonos = 0;
                totalsaldos = 0;
                datos = [];
                datos[0] = ['Fecha', 'Vr.Total', 'Abonos', 'Saldo'];
                datos2 = [];
                datos2[0] = ['Fecha', 'Número de órdenes para la fecha'];
                for (var i = 0; i < data.length; i++) {
                    totalabonos += parseInt(data[i][2]);
                    totalsaldos += parseInt(data[i][3]);
                    data[i].pop();
                    datos2.push([data[i][0], parseInt(data[i][4])]);
                    data[i].pop();
                    for (var a = 1; a < data[i].length; a++) {
                        data[i][a] = parseInt(data[i][a]);
                    }
                    datos.push(data[i]);
                }
                var datag = google.visualization.arrayToDataTable(datos);
                var datag2 = google.visualization.arrayToDataTable(datos2);
                var options = {
                    chart: {
                        title: 'Cartera',
                        subtitle: 'Vr.Total, Abonos, Saldo',
                        width: $('#columnchart_material ').width(),
                        height: $('#columnchart_material ').height()
                    },
                    vAxis: { format: '$' },
                    colors: ['#1b9e77', '#d95f02', '#7570b3']
                };
                var options2 = {
                    chart: {
                        title: 'Número de órdenes por día',
                        subtitle: 'Consolidado',
                        width: $('#columnchart_material2 ').width(),
                        height: $('#columnchart_material2 ').height()
                    },
                    colors: ['#7570ff']
                };
                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
                chart.draw(datag, google.charts.Bar.convertOptions(options));
                var chart2 = new google.charts.Bar(document.getElementById('columnchart_material2'));
                chart2.draw(datag2, google.charts.Bar.convertOptions(options2));
                drawChart(totalabonos, totalsaldos);
            });

        },
    });
});

function recarga() {
    url_ = 'reporte_cartera.php?fecha1=' + $("#fecha1").val() + "&fecha2=" + $("#fecha2").val();
    location.href = url_;
}

function drawChart(totalabonos, totalsaldos) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'variable');
    data.addColumn('number', 'valor');
    data.addRows([
        ['Abonos', totalabonos],
        ['Saldos', totalsaldos]
    ]);

    var options = {
        'title': 'Abonos vs Saldos',
        'width': 400,
        'height': 300,
        colors: ['#d95f02', '#7570b3']
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}