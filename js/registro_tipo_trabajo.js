$('document').ready(function()
{ 
    $("#form_tipo_trabajo").validate({
        rules: {
         nombre: {
             required: true
             //validname: true
         },
         descripcion: {
             required: true
         },
         valor: {
           required: true,
           digits: true
         },
         tiempo: {
            required: true,
            digits: true
          }
       },
       messages: {
        nombre: {
            required: "Por favor ingrese un nombre"
            //validname: "Ingrese un nombre válido"
        },
        descripcion: {
           required: "Por favor ingrese una descripción para este tipo de trabajo"
        },
        valor: {
           required: "Ingrese un valor predeterminado para este tipo de trabajo",
           digits: 'Sólo números'
        },
        tiempo: {
            required: "Ingrese un tiempo predeterminado para la realización de este trabajo",
            digits: 'Sólo números'
        }
       },
       /*errorPlacement : function(error, element) {
           $(element).closest('.form-group').find('.help-block').html(error.html());
           },
           highlight : function(element) {
           $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
           },
           unhighlight: function(element, errorClass, validClass) {
           $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
           $(element).closest('.form-group').find('.help-block').html('');
           },*/
       errorPlacement : function(error, element) 
       {
           $(element).next('.help-block').html(error.html());
           },
           highlight : function(element) {
           $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
           },
           unhighlight: function(element, errorClass, validClass) {
           $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
           $(element).next('.help-block').html('');
       },
       submitHandler: function(form)
       {
           form.submit();
       }
    });

    var exito = getParameterByName('exito');
    if (exito == "1")
    {
        $("#exito").show();
        setTimeout("$('#exito').hide();",3000);
    }
    else if(exito == "0")
    {
        $("#falla").show();
        setTimeout("$('#falla').hide();",3000);
    }
});

function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}