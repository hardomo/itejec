var oTable;
let totalabonos = 0;

$('document').ready(function() {
    oTable = $('#detalle_abonos').dataTable({
        "sDom": 'T<"clear">lfrtip',
        "bAutoWidth": true,
        "paging": false,
        "bFilter": false,
        "bProcessing": true,
        "bServerSide": false,
        "aaSorting": [
            [0, "asc"]
        ],
        "bInfo": false,
        "sAjaxSource": "./tablas/abonos_fecha.php?fecha=" + $("#fecha").val(),
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sCopy": "Se han copiado los datos al portapapeles"
        },
        "footerCallback": function(row, data, start, end, display) {
            var api = this.api();
            nb_cols = api.columns().nodes().length;
            var j = 1;
            while (j < nb_cols) {
                var pageTotal = api
                    .column(j, { page: 'current' })
                    .data()
                    .reduce(function(a, b) {
                        return Number(a) + Number(b);
                    }, 0);
                // Update footer
                $(api.column(j).footer()).html(pageTotal);
                if (j == 1) {
                    totalabonos = pageTotal;
                }
                j++;
            }
        }
    });
});