var oTable;
$('document').ready(function()
{ 
    oTable = $('#usuarios').dataTable({
        "aaSorting": [[ 0, "asc" ]],
        "bInfo": false,
        "paging": false,
        "sAjaxSource": "./tablas/usuarios.php",
        "oLanguage":{
            "sProcessing":   "Procesando...",
            "sZeroRecords":  "No se encontraron resultados",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "sCopy":		 "Se han copiado los datos al portapapeles"
        }
    });
});

function elimina_usuario(idusuario)
{
    $('#dialog').modal('hide');
    url_= './controller/crea_usuario.php';
    $.ajax({
        type: "GET",
        method: "GET",
        data: { idusuarioborrar: $("#idusuarioborrar").attr("value"), action:2},
        url: url_,
        success: function(datos)
        {
          oTable._fnAjaxUpdate();
        }
    });
}

function confirma_eliminar(idusuario)
{
    $("#idusuarioborrar").attr("value",idusuario);
    $('#dialog').modal('show');
}