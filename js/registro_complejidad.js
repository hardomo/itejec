$('document').ready(function()
{ 
    $("#form_complejidad").validate({
        rules: {
         nombre: {
             required: true
         },
         incremento_porcentual: {
             required: true,
             digits: true
         },
         incremento_tiempo: {
           required: true,
           digits: true
         }
       },
       messages: {
        nombre: {
            required: "Por favor ingrese un nombre para esta complejidad"
        },
        incremento_porcentual: {
           required: "Por favor ingrese el incremento porcentual en el valor cobrado",
           digits: 'Sólo números'
        },
        incremento_tiempo: {
           required: "Ingrese los minutos de incremento que agrega esta complejidad al trabajo",
           digits: 'Sólo números'
        }
       },
       errorPlacement : function(error, element) 
       {
           $(element).next('.help-block').html(error.html());
           },
           highlight : function(element) {
           $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
           },
           unhighlight: function(element, errorClass, validClass) {
           $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
           $(element).next('.help-block').html('');
       },
       submitHandler: function(form)
       {
           form.submit();
       }
    });

    var exito = getParameterByName('exito');
    if (exito == "1")
    {
        $("#exito").show();
        setTimeout("$('#exito').hide();",3000);
    }
    else if(exito == "0")
    {
        $("#falla").show();
        setTimeout("$('#falla').hide();",3000);
    }
});

function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}