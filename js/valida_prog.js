function desprogramar_item(item)
{
  $.ajax(
    {
      data: {id: item},
      url: './controller/desprograma.php',
      success: function(respuesta) 
      {
        recalcula_tiempo();
        $('#detalle_prog_validar').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}

function recalcula_tiempo()
{
    $.ajax(
    {
      data: {idprog: idprog},
      url: './controller/recalcula_t.php',
      success: function(respuesta) 
      {
        thoras = (respuesta/60).toFixed(2);
        $('#tiempo_total').html(respuesta+ " minutos ("+thoras+" horas)");
      },
      error: function (request, status, error) 
      {}
    });
}

function validar_prog()
{
  $.ajax(
    {
      data: {idprog: idprog},
      url: './controller/valida_prog.php',
      success: function(respuesta) 
      {
        $('#detalle_prog_validar').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}

function terminar_prog()
{
  $.ajax(
    {
      data: {idprog: idprog},
      url: './controller/termina_prog.php',
      success: function(respuesta) 
      {
        $('#detalle_prog_validar').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}