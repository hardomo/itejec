function reprogramar_item(item,fecha)
{
  $.ajax(
    {
      data: {id: item, fecha:fecha},
      url: './controller/reprograma.php',
      success: function(respuesta) 
      {
        $('#detalle_prog_validar').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}

function terminar_prog()
{
  $.ajax(
    {
      data: {idprog: idprog},
      url: './controller/termina_prog.php',
      success: function(respuesta) 
      {
        $('#detalle_prog_validar').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}

function reprogramar_items()
{
    /*items_eliminar = array();
    items_eliminar = $("checkbox");*/
    //items_terminar = $('#detalle_prog_validar > input[type=checkbox]').prop('checked', $(this).is(':checked'));
    items_reprogramar = new Array();
    //$('input[type=checkbox]').prop('checked', $(this).is(':checked')).each(function(){
    $("input[type=checkbox]:unchecked").each(function()
    {
      items_reprogramar.push($(this).attr('id'));
    });
    $.ajax(
    {
      data: {items_reprogramar:items_reprogramar},
      url: './controller/reprograma_items.php',
      success: function(respuesta)
      {
        //$('#detalle_prog_validar').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}

function termina_seleccion()
{
    items_terminar = new Array();
    $("input[type=checkbox]:checked").each(function()
    {
      items_terminar.push($(this).attr('id'));
    });
    $.ajax(
    {
      data: {items_terminar:items_terminar},
      url: './controller/termina_items.php',
      success: function(respuesta)
      {
        $('#detalle_prog_validar').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}