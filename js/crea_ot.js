function trae_detalles_cliente(idcliente)
{
  $.ajax(
    {
      data: {id: idcliente},
      url: './controller/detalles_cliente.php',
      dataType: 'json',
      success: function(respuesta) 
      {
        $('#nombre_cliente').html(respuesta[0]['nombre']);
        $('#cedula').html(respuesta[0]['cedula']);
        $('#correo_e').html(respuesta[0]['correo_e']);
        $('#direccion').html(respuesta[0]['direccion']);
        $('#telefono1').html(respuesta[0]['telefono1']);
        $('#telefono2').html(respuesta[0]['telefono2']);
        $('#fecha_nac').html(respuesta[0]['fecha_nacimiento']);
      },
      error: function (request, status, error) 
      {
      }
    });
}

function preparar_ot(idot)
{
  if(idot == 0)
  {
    url = "registro_prenda.php?idot=0&cliente="+$('#cliente').val()+"&fingreso="+$('#fecha').val()+"&fentrega="+$('#fecha_entrega').val();
  }
  else
  {
    url = "registro_prenda.php?idot="+idot;
  }
  window.location.replace(url);
}

function entrega_ot()
{
  var saldo_pendiente = saldoPendiente;
  $.ajax(
    {
      data: {idot: idot},
      url: './controller/entrega_ot.php?saldo='+saldo_pendiente,
      success: function(respuesta)
      {
        //$('#estado_ot').html("Entregado");
        //$('#bentrega').attr("disabled", true);
        //$('#detalle_ot').dataTable()._fnAjaxUpdate();
        $('#dialog3').modal('show');
        setTimeout(function(){$('#dialog3').modal('hide');}, 3000);
        location.reload();
      },
      error: function (request, status, error) 
      {}
    });
}

function actualizar_f_entrega(fecha)
{
  $.ajax(
  {
    data: {fecha: fecha},
    url: './controller/existe_programacion.php',
    dataType: 'json',
    success: function(respuesta) 
    {
      if(respuesta[0])
      {
        var enlace = "registro_programacion.php?idprog="+respuesta[0]['id'];
        $("#ver_prog").attr("href",enlace);
      }
      else
      {
        var enlace = "registro_programacion.php?idprog=0";
        $("#ver_prog").attr("href",enlace);
        $('#dialog5').modal('show');
        setTimeout(function(){$('#dialog5').modal('hide');}, 2000);
      }
    },
    error: function (request, status, error) 
    {
    }
  });

  if(idot)
  {
    $.ajax(
    {
      data: {idot: idot, fecha_entrega: fecha},
      url: './controller/edita_ot.php',
      dataType: 'json',
      success: function(respuesta) 
      {
      },
      error: function (request, status, error) 
      {
      }
    });
  }
}

function printData()
{
  qz.websocket.connect().then(function() { 
    return qz.printers.find("POS");              // Pass the printer name into the next Promise
    }).then(function(printer) {

      var config = qz.configs.create(printer);       // Create a default config for the found printer
      var P_prendas = document.getElementById("Pitems_imprimir").innerHTML; 
      var P_orden = document.getElementById("Pidot_p").innerHTML;
      var P_estado_ot = document.getElementById("Pestado_ot").innerHTML;
      var P_fecha_orden = document.getElementById("Pfecha_orden").innerHTML;
      var P_fecha_entrega = document.getElementById("Pfecha_entrega").innerHTML;
      var P_nombrecliente = document.getElementById("Pnombrecliente").innerHTML;
      var P_cedula = document.getElementById("Pcedula").innerHTML;
      var P_direccion = document.getElementById("Pdireccion").innerHTML;
      var P_total = document.getElementById("Ptotal").innerHTML;
      var P_abonos = document.getElementById("Pabonos").innerHTML;
      var P_saldo = document.getElementById("Psaldo").innerHTML;
      var P_fecha_actual = document.getElementById("Pfecha_actual").innerHTML;

      var data = [
          '\x1B' + '\x40',          // init
          '\x1B' + '\x61' + '\x31', // center align
          '\x1D' + '\x21' + '\x11', // double font size
          "ilo",
          '\x0A' + '\x0A',
          '\x1D' + '\x21' + '\x00', // standard font size
          '\x1B' + '\x61' + '\x31', // center align
          "Estrena tu ropa usada",
          '\x0A' + '\x0A',
          '\x1D' + '\x21' + '\x00', // standard font size
          '\x1B' + '\x61' + '\x30', // left align
          'Fecha: ' + P_fecha_actual,
          '\x0A' + '\x0A',
          '\x1B' + '\x45' + '\x0D', // bold on
          'Cliente: ' + P_nombrecliente,
          '\x0A',
          '\x1B' + '\x45' + '\x0A', // bold off
          'Documento: ' + P_cedula,
          '\x0A',
          'Dirección: ' + '\x0A' + P_direccion,
          '\x0A' + '\x0A',
          '\x1B' + '\x45' + '\x0D', // bold on
          'Orden de trabajo: ' + P_orden,
          '\x0A',
          'Estado: ' + P_estado_ot,
          '\x0A',
          '\x1B' + '\x45' + '\x0A', // bold off
          'Fecha de ingreso: ' + P_fecha_orden,
          '\x0A',
          'Fecha de entrega: ' + P_fecha_entrega,
          '\x1B' + '\x45' + '\x0A', // bold off
          P_prendas,
          '\x0A' + '\x0A',
          '\x1B' + '\x45' + '\x0D', // bold on
          '\x1B' + '\x61' + '\x31', // center align
          "Valor total y abonos",
          '\x0A' + '\x0A',
          '\x1B' + '\x45' + '\x0A', // bold off
          '\x1B' + '\x61' + '\x30', // left align
          'Valor total: ' + P_total,
          '\x0A',
          'Total abonos: ' + P_abonos,
          '\x0A',
          '\x1B' + '\x45' + '\x0D', // bold on
          'Saldo Pendiente: ' + P_saldo,
          '\x1B' + '\x45' + '\x0A', // bold off
          '\x1D' + '\x21' + '\x00', // standard font size
          '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A',
          '\x1B' + '\x69',          // cut paper (old syntax)
          ];   // Raw ZPL

          return qz.print(config, data);
      }).then(function(){
          return qz.websocket.disconnect();
      }).catch(function(e) { console.error(e); });
}

function elimina_item(iditem)
{
    $('#dialog').modal('hide');
    url_= './controller/prenda_ot.php';
    $.ajax({
        type: "GET",
        method: "GET",
        data: { iditem: $("#iditemborrar").attr("value"), action:2, idot:idot},
        url: url_,
        success: function(datos)
        {
          $('#detalle_ot').dataTable()._fnAjaxUpdate();
        }
    });
}

function editar_abono(idabono)
{
  $("#valorabono").val($("#valor"+idabono).html());
  $("#fechaabono").val($("#fecha"+idabono).html());
  $("#idabonoeditar").val(idabono);
  $("#action").val(1);
  $('#dialog2').modal('show');
}

/*function pet_editar_abono(idabono)
{   
  $('#dialog2').modal('hide');
  url_= './controller/abono_ot.php';
  $.ajax({
      type: "GET",
      method: "GET",
      data: { idabono: idabono,
      valorabono: $("#valorabono").attr("value"),
      fechaabono: $("#fechaabono").attr("value"),
      action:1},
      url: url_,
      success: function(datos)
      {
        oTable2._fnAjaxUpdate();
      }
  });
  $("#idabonoeditar").val("");
}*/

function confirma_eliminar(iditem)
{
    $("#iditemborrar").attr("value",iditem);
    $('#dialog').modal('show');
}

$('document').ready(function()
{    
    $("#form_abono").validate({
     rules: {
      fechaabono: {
          date: true,
          required: true
      },
      valorabono: {
          required: true,
        digits: true
      }
    },
    messages: {
     fechaabono: {
         date: "Ingrese una fecha válida",
         required: "Por favor ingrese la fecha"
     },
     valorabono: {
        required: "Ingrese el valor del abono",
        digits: 'Sólo números'
       }
    },
    errorPlacement : function(error, element) 
    {
        $(element).next('.help-block').html(error.html());
        },
        highlight : function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(element).next('.help-block').html('');
    },
    submitHandler: function(form)
    {
        $('#dialog2').modal('hide');
        form.submit();
        //printData("tiquete");
        $("#idabonoeditar").val("");
        $("#action").val("");
    }
   });
});

function confirma_ot(idot)
{
  if(idot == 0 || $('#fecha_entrega').val().length <10)
  {
    $('#dialog4').modal('show');
  }
  else
  {
    fecha_entrega = $('#fecha_entrega').val();
    $.ajax(
      {
        data: {idot: idot, fecha_entrega:fecha_entrega, estado: "Confirmado"},
        url: './controller/cambia_estado_ot.php',
        success: function(respuesta)
        {
          location.reload();
          /*$('#estado_ot').html("Confirmado");
          $('#detalle_ot').dataTable()._fnAjaxUpdate();
          $('#b_confirmaot').hide();
          $('#bap').addClass("disabled");*/
        },
        error: function (request, status, error) 
        {}
      });
  }
}