function desprogramar_item(item)
{
  $.ajax(
    {
      data: {id: item},
      url: './controller/desprograma.php',
      success: function(respuesta) 
      {
        recalcula_tiempo();
        $('#items_pendientes').dataTable()._fnAjaxUpdate();
        $('#detalle_prog').dataTable()._fnAjaxUpdate();
      },
      error: function (request, status, error) 
      {}
    });
}

function recalcula_tiempo()
{
    $.ajax(
    {
      data: {idprog: idprog},
      url: './controller/recalcula_t.php',
      success: function(respuesta) 
      {
        thoras = (respuesta/60).toFixed(2);
        $('#tiempo_total').html(respuesta+ " minutos ("+thoras+" horas)");
      },
      error: function (request, status, error) 
      {}
    });
}

function programar_item(item)
{
  $.ajax(
  {
    data: {id_item: item, id_prog:idprog},
    url: './controller/agrega_prog.php',
    success: function(respuesta) 
    {
      recalcula_tiempo();
      $('#detalle_prog').dataTable()._fnAjaxUpdate();
      $('#items_pendientes').dataTable()._fnAjaxUpdate();
    },
    error: function (request, status, error) 
    {}
  });
}

function agrega_prendas_dia(fecha)
{
  $.ajax(
  {
    data: {fecha: fecha},
    url: './controller/programa_dia.php',
    success: function(respuesta) 
    {
      recalcula_tiempo();
      $('#items_pendientes').dataTable()._fnAjaxUpdate();
      $('#detalle_prog').dataTable()._fnAjaxUpdate();
    },
    error: function (request, status, error) 
    {}
  });
}