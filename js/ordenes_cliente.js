var oTable;

$('document').ready(function()
{
    $('#cliente').change();
});

function trae_detalles_cliente(idcliente)
{
  $.ajax(
    {
        data: {id: idcliente},
        url: './controller/detalles_cliente.php',
        dataType: 'json',
        success: function(respuesta) 
        {
        $('#nombre_cliente').html(respuesta[0]['nombre']);
        $('#cedula').html(respuesta[0]['cedula']);
        $('#correo_e').html(respuesta[0]['correo_e']);
        $('#direccion').html(respuesta[0]['direccion']);
        $('#telefono1').html(respuesta[0]['telefono1']);
        $('#telefono2').html(respuesta[0]['telefono2']);
        $('#fecha_nac').html(respuesta[0]['fecha_nacimiento']);
        },
        error: function (request, status, error) 
        {
        }
    });

    if(oTable != null)
    {
        oTable.fnDestroy();
    }
    oTable = $('#detalle_ot').dataTable({
        "sDom": 'T<"clear">lfrtip',
        "bAutoWidth": true,
        "paging":   false,
        "bFilter": false,
        "bProcessing": true,
        "bServerSide": false,
        "aaSorting": [[ 0, "asc" ]],
        "bInfo": false,
        "sAjaxSource": "./tablas/ordenes_trabajo_cliente.php?idcliente="+$("#cliente").val(),
        "oLanguage":{
            "sProcessing":   "Procesando...",
            "sZeroRecords":  "No se encontraron resultados",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "sCopy":		 "Se han copiado los datos al portapapeles"
        },
        "footerCallback": function ( row, data, start, end, display ) 
        {
            var api = this.api();
            nb_cols = api.columns().nodes().length;
            var j = 3;
            while(j < nb_cols)
            {
                var pageTotal = api
                .column( j, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return Number(a) + Number(b);
                }, 0 );
                // Update footer
                $( api.column( j ).footer() ).html(pageTotal);
                j++;
            } 
        }
    });
}

function confirma_eliminar(idot)
{
    $("#idotborrar").attr("value",idot);
    $('#dialog').modal('show');
}

function elimina_ot(idot)
{
    $('#dialog').modal('hide');
    url_= './controller/elimina_ot.php';
    $.ajax({
        type: "GET",
        method: "GET",
        data: { idotborrar: $("#idotborrar").attr("value"), action:2},
        url: url_,
        success: function(datos)
        {
          //oTable._fnAjaxUpdate();
          window.location = datos;
        }
    });
}