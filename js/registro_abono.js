$('document').ready(function()
{    
    $("#form_abono").validate({
     rules: {
      fecha: {
          date: true,
          required: true
      },
      valor: {
          required: true,
        digits: true
      }
    },
    messages: {
     fecha: {
         date: "Ingrese una fecha válida",
         required: "Por favor ingrese la fecha"
     },
     valor: {
        required: "Ingrese el valor del abono",
        digits: 'Sólo números'
       }
    },
    errorPlacement : function(error, element) 
    {
        $(element).next('.help-block').html(error.html());
        },
        highlight : function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(element).next('.help-block').html('');
    },
    submitHandler: function(form)
    {
        printData("tiquete");
        form.submit();
    }
   });
});

function printData(elemento)
{
   var divToPrint = document.getElementById(elemento);
   console.log(elemento);
   newWin= window.open("");
   //newWin.document.write(divToPrint.outerHTML);
   newWin.document.write(divToPrint.innerHTML);
   newWin.print();
   newWin.close();
}