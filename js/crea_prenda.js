$('document').ready(function()
{    
  $("#form_item").validate({
     rules: {
      tipo_prenda: {
          required: true
      },
      tipo_trabajo: {
          required: true
      },
      complejidad: {
        required: true
      },
      valor: {required: true}
    },
    messages: {
     tipo_prenda: {
         required: "Seleccione el tipo de prenda"
     },
     tipo_trabajo: {
      required: "Seleccione el tipo de trabajo a realizar"
      },
    complejidad: {
        required: "Seleccione la complejidad del trabajo a realizar"
     },
     valor:{
         required: "Debe asignar un valor a este trabajo"
     }
    },
    errorPlacement : function(error, element) 
    {
        $(element).next('.help-block').html(error.html());
        },
        highlight : function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(element).next('.help-block').html('');
    },
    submitHandler: function(form)
    {
      //printData("etiqueta");
      form.submit();
    }
  });

  if(iditem != 0)
  {
    $("#tipo_prenda option[value="+ tipo_prenda +"]").attr("selected",true);
    $("#tipo_prenda").change();
    $("#tipo_trabajo option[value="+ tipo_trabajo +"]").attr("selected",true);
    $("#tipo_trabajo").change();
    $("#complejidad option[value="+ complejidad +"]").attr("selected",true);
    $("#observaciones").blur();
    $("#valor").val(valor);
  }
});

function calcular_valor()
{
  var optionSelectedcomp = $("#complejidad").find("option:selected");
  var porcentaje = parseInt(optionSelectedcomp.attr('porc'));
  var precio = parseInt($("#tipo_trabajo option:selected").attr('precio'));
  $("#valor").val(precio+(precio*porcentaje/100));
}

function printData()
{
  qz.websocket.connect().then(function() { 
    return qz.printers.find("ETIQUETAS");              // Pass the printer name into the next Promise
    }).then(function(printer) {
      var config = qz.configs.create(printer);       // Create a default config for the found printer
        var Pprenda = document.getElementById("Ptprenda_p").innerHTML; 
        var Porden = document.getElementById("Pidot_p").innerHTML;
        var Ptrabajo = document.getElementById("Pttrabajo_p").innerHTML;
        var Pobservaciones = document.getElementById("Pobs_p").innerHTML;
        var Pcolor = document.getElementById("Pcolor_p").innerHTML;
        var Pnombre = document.getElementById("Pnombre_p").innerHTML;
        var Pmarca = document.getElementById("Pmarca_p").innerHTML;

        //IMpresión para etiqueta de 70 x 32 mm
        var data = [
          '^XA\n',
          '^CF0,25',
          '^FO152,5^FD' + Pnombre + '^FS',
          '^FO152,35^FD' + Pprenda + '^FS',
          '^FO152,65^FDColor:' + Pcolor + '^FS',
          '^FO152,95^FDMarca:' + Pmarca + '^FS',
          '^FO152,125^FDOrden N.:' + Porden + '^FS',
          '^FO152,155',
          '^FB545,2,,',
          '^FD' + Ptrabajo + '^FS',
          '^FO152,205',
          '^FB545,3,,',
          '^FD*' + Pobservaciones +'*^FS',
          {
            type: 'raw',
            data: '',
          }, 
          '^FS\n',
          '^XZ\n'
        ];
          return qz.print(config, data);
        }).then(function(){
            return qz.websocket.disconnect();
        }).catch(function(e) { console.error(e); });
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

function select_tt(tprenda)
{
  $('#tprenda_p').html($(this).find('option:selected').text());
  $("#tipo_trabajo").empty();
  $.ajax(
    {
      data: {tprenda:tprenda},
      url: './controller/consulta_tt_tp.php',
      success: function(respuesta) 
      {
        listaopciones = "";
        listaopciones += respuesta;
        $("#tipo_trabajo").append(listaopciones);
        $("#tipo_trabajo option[value="+ tipo_trabajo +"]").attr("selected",true);
        $("#tipo_trabajo").change();
      },
      error: function (request, status, error) 
      {
      }
    });
}