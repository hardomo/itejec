var oTable;
let totalabonos = 0;
let totalsaldos = 0;

$('document').ready(function() {
    oTable = $('#detalle_ot').dataTable({
        "sDom": 'T<"clear">lfrtip',
        "bAutoWidth": true,
        "paging": false,
        "bFilter": false,
        "bProcessing": true,
        "bServerSide": false,
        "aaSorting": [
            [0, "asc"]
        ],
        "bInfo": false,
        "sAjaxSource": "./tablas/ordenes_trabajo_fecha.php?fecha=" + $("#fecha").val(),
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sCopy": "Se han copiado los datos al portapapeles"
        },
        "footerCallback": function(row, data, start, end, display) {
            var api = this.api();
            nb_cols = api.columns().nodes().length;
            var j = 4;
            while (j < nb_cols) {
                var pageTotal = api
                    .column(j, { page: 'current' })
                    .data()
                    .reduce(function(a, b) {
                        return Number(a) + Number(b);
                    }, 0);
                // Update footer
                $(api.column(j).footer()).html(pageTotal);
                if (j == 5) {
                    totalabonos = pageTotal;
                } else if (j == 6) {
                    totalsaldos = pageTotal;
                }
                j++;
            }
        },
        "initComplete": function() {
            google.charts.setOnLoadCallback(function() {
                drawChart(totalabonos, totalsaldos);
            });

        },
    });
});

function drawChart(totalabonos, totalsaldos) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'variable');
    data.addColumn('number', 'valor');
    data.addRows([
        ['Abonos', totalabonos],
        ['Saldos', totalsaldos]
    ]);

    var options = {
        'title': 'Abonos vs Saldos',
        'width': 400,
        'height': 300,
        colors: ['#d95f02', '#7570b3']
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}