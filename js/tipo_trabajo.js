var oTable;
$('document').ready(function()
{ 
    oTable = $('#tipos_trabajo').dataTable({
        "aaSorting": [[ 0, "asc" ]],
        "bInfo": false,
        "paging": false,
        "sAjaxSource": "./tablas/tipos_trabajo.php",
        "oLanguage":{
            "sProcessing":   "Procesando...",
            "sZeroRecords":  "No se encontraron resultados",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "sCopy":		 "Se han copiado los datos al portapapeles"
        }
    });

    var exito = getParameterByName('exito');
    if(exito == "0")
    {
        $("#falla").show();
        setTimeout("$('#falla').hide();",3000);
    }
});

function elimina_tt(idtt)
{
    $('#dialog').modal('hide');
    url_= './controller/crea_tipo_trabajo.php';
    $.ajax({
        type: "GET",
        method: "GET",
        data: { idttborrar: $("#idttborrar").attr("value"), action:2},
        url: url_,
        success: function(datos)
        {
          //oTable._fnAjaxUpdate();
          window.location = datos;
        }
    });
}

function confirma_eliminar(idtt)
{
    $("#idttborrar").attr("value",idtt);
    $('#dialog').modal('show');
}

function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}