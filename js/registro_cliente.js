//usuario_no_existe = false;

$('document').ready(function()
{    
   // name validation
   /* var nameregex = /^[a-zA-Z ]+$/;
   
   $.validator.addMethod("validname", function( value, element ) {
       return this.optional( element ) || nameregex.test( value );
   }); 
   
   // valid email pattern
   var eregex = /^([a-zA-Z0-9_.-+])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
   
   $.validator.addMethod("validemail", function( value, element ) {
       return this.optional( element ) || eregex.test( value );
   });*/

    $("#form_cliente").validate({
     rules: {
      nombre: {
          required: true
      },
      cedula: {
          required: true,
          minlength: 7,
          digits: true
      },
      direccion: {required: true},
      telefono1: {
        required: false,
        minlength: 7,
        digits: true
      },
      telefono2: {
        minlength: 7,
        digits: true
      },
      correo: {
       required: true,
       email: true
       //validemail: true,
      },
      fecha_nac:{
          date: true
      }
    },
    messages: {
     nombre: {
         required: "Por favor ingrese nombre",
         pattern: "Ingrese un nombre válido"
     },
     cedula: {
        required: "Por favor ingrese número de teléfono",
        digits: 'Sólo números'
     },
     direccion:{
         required: "Ingrese dirección de residencia"
     },
     telefono1: {
        required: "Ingrese un teléfono",
        digits: 'Sólo números',
        minlength: "Mínimo 7 dígitos"
       },
       telefono2: {
        digits: 'Sólo numeros',
        minlength: "Mínimo 7 dígitos"
       },
     correo: {
         required: "Ingrese un correo electrónico",
         email: "Ingrese un correo válido"
         //validemail: "Ingrese un correo electrónico válido"
     },
     fecha_nac:{
         date: "Ingrese una fecha válida"
     }
    },
    /*errorPlacement : function(error, element) {
        $(element).closest('.form-group').find('.help-block').html(error.html());
        },
        highlight : function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(element).closest('.form-group').find('.help-block').html('');
        },*/
    errorPlacement : function(error, element) 
    {
        $(element).next('.help-block').html(error.html());
        },
        highlight : function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(element).next('.help-block').html('');
    },
    submitHandler: function(form)
    {
      //return false;
        /*var pass = $("#password").val();
        var passwordBytes = CryptoJS.enc.Utf16LE.parse(pass);
        var sha1Hash = CryptoJS.SHA1(passwordBytes);
        var sha1HashToBase64 = sha1Hash.toString(CryptoJS.enc.Base64);
        $("#password").html(sha1HashToBase64);*/
        form.submit();
    }
   });

   var exito = getParameterByName('exito');
    if (exito == "1")
    {
        $("#exito").show();
        setTimeout("$('#exito').hide();",3000);
    }
    else if(exito == "0")
    {
        $("#falla").show();
        setTimeout("$('#falla').hide();",3000);
    }
});

function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

  /* $.validator.addMethod("verifica_usuario", function(value, element) {
    if(value == 2|| value == 5 || value == 8){
     return false;
    }else{
     return true;
    }
   }, "Enter value other than 2,5, and 8");*/

/*
function verifica_password(c1,c2)
{
    if(c1==c2)
    {
        return true;
    }
    else
    {
        return false;
    }
}*/

/*function verifica_usuario(usuario)
{
    $.ajax(
    {
        url : '.\bd\consultas_usuarios.php',
        type: "POST",
        data : {usuario: usuario},
        success: function(datos)
        {
            if(datos!=0)
            {
                $("#usuario_ya_existe").show();
                $('#boton_enviar').attr("disabled", true);
            }
            else
            {
                $("#usuario_ya_existe").hide();
            }
            console.log(datos);
        },
        error:function(datos)
        {
            alert("No se pudo hacer la consulta del usuario");
        }
    });
}

function verifica_passwords(p1,p2)
{
    if(p1!=p2)
    {
        $("#password_no_concuerda").show();
        //$('#boton_enviar').attr("disabled", true);
    }
    else
    {
        $("#password_no_concuerda").hide();
    }
}

function verifica_formulario_completo()
{

}

function verifica_datos()
{
    if(datos_completos && usuario_no_existe && password_concuerda)
    {
        $('#boton_enviar').attr("disabled", false);
    }
}*/