var oTable;
$('document').ready(function()
{
    oTable = $('#contenedores').dataTable({
        "aaSorting": [[ 0, "asc" ]],
        "bInfo": false,
        "paging": false,
        "sAjaxSource": "./tablas/contenedores.php",
        "oLanguage":{
            "sProcessing":   "Procesando...",
            "sZeroRecords":  "No se encontraron resultados",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "sCopy":		 "Se han copiado los datos al portapapeles"
        }
    });

    var exito = getParameterByName('exito');
    if(exito == "0")
    {
        $("#falla").show();
        setTimeout("$('#falla').hide();",3000);
    }
});

function elimina_contenedor(idcontenedor)
{
    $('#dialog').modal('hide');
    url_= './controller/crea_contenedor.php';
    $.ajax({
        type: "GET",
        method: "GET",
        data: { idcontborrar: $("#idcontborrar").attr("value"), action:2},
        url: url_,
        success: function(datos)
        {
          //oTable._fnAjaxUpdate();
          window.location = datos;
        }
    });
}

function confirma_eliminar(idcontenedor)
{
    $("#idcontborrar").attr("value",idcontenedor);
    $('#dialog').modal('show');
}

function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}