var oTable;
$('document').ready(function() {

    oTable = $('#ingresos').DataTable({
        "aaSorting": [
            [0, "asc"]
        ],
        "bInfo": false,
        "sAjaxSource": "./tablas/ingresos_fechas.php?fecha1=" + $("#fecha1").val() + "&fecha2=" + $("#fecha2").val(),
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sCopy": "Se han copiado los datos al portapapeles",
            "oPaginate": {
                "sFirst": "Primero",
                "sPrevious": "Anterior",
                "sNext": "Siguiente",
                "sLast": "&Uacute;ltimo",
            }
        },
        "initComplete": function() {
            google.charts.setOnLoadCallback(function() {

                var data = [];
                if (oTable) {
                    var rows = oTable.rows();
                    data = rows.data();
                } else {
                    console.log(this.IDENTITY + " -> getData() table undefined " + table);
                }

                datos = [];
                datos[0] = ['Fecha', 'Dinero Recaudado'];

                for (var i = 0; i < data.length; i++) {
                    data[i].pop();
                    data[i].pop();
                    for (var a = 1; a < data[i].length; a++) {
                        data[i][a] = parseInt(data[i][a]);
                    }
                    datos.push(data[i]);
                }
                var datag = google.visualization.arrayToDataTable(datos);

                var options = {
                    chart: {
                        title: 'Ingresos',
                        subtitle: 'Vr.Total',
                        width: $('#columnchart_material ').width(),
                        height: $('#columnchart_material ').height()
                    },
                    vAxis: { format: '$' },
                    colors: ['#1b9e77']
                };

                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
                chart.draw(datag, google.charts.Bar.convertOptions(options));
            });
        },
    });
});

function recarga() {
    url_ = 'reporte_ingresos_fechas.php?fecha1=' + $("#fecha1").val() + "&fecha2=" + $("#fecha2").val();
    location.href = url_;
}